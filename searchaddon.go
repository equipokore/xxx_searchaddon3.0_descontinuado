package main

import (
	"fmt"
	"net/http"

	"./Controladores"
	"./Controladores/Buscar"
	"github.com/gorilla/mux"
)

//############################################### F  U  N  C  I  O  N  E S ########################################################
func makeHandler(fn func(w http.ResponseWriter, r *http.Request)) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		fn(w, r)
	}
}

var router = mux.NewRouter()

//##################################################   M   A   I   N   ############################################################
func main() {
	//Router Mux
	http.Handle("/", router)

	http.Handle("/Recursos/", http.StripPrefix("/Recursos/", http.FileServer(http.Dir("Recursos"))))
	//Index
	router.HandleFunc("/", makeHandler(controllers.Index))
	router.HandleFunc("/index", makeHandler(controllers.Index))
	//Busqueda
	router.HandleFunc("/busqueda", makeHandler(buscar.Busqueda))
	router.HandleFunc("/busqueda/{p}", makeHandler(buscar.Busqueda))

	router.HandleFunc("/modificararticulo", makeHandler(buscar.Modificar))
	router.HandleFunc("/guardarimgdearticulo", makeHandler(buscar.Imagen))
	router.HandleFunc("/guardararchdearticulo", makeHandler(buscar.Archivo))
	router.HandleFunc("/del", makeHandler(buscar.EliminarDirectorio))
	router.HandleFunc("/del2", makeHandler(buscar.EliminarDirectorio2))
	router.HandleFunc("/eliminarimg", makeHandler(buscar.EliminarImagen))

	//router.HandleFunc("/articulodeelastic", makeHandler(buscar.GetFromElasticSearch))

	//Login
	router.HandleFunc("/login", makeHandler(controllers.Login))
	router.HandleFunc("/logout", makeHandler(controllers.Logout))

	//PassRecovery
	router.HandleFunc("/passwordrecovery", makeHandler(controllers.RecuperacionContraseña))

	//Create Users
	router.HandleFunc("/registrarse", makeHandler(controllers.CrearUsuario))

	//ModifcarCuenta
	router.HandleFunc("/AdministrarCuenta/{ids}", makeHandler(controllers.ModificarCuenta))

	//Administrar Usuarios
	router.HandleFunc("/AdministrarUsuarios", makeHandler(controllers.AdminUsers))
	//Server Run
	server := http.Server{
		Addr: ":9096",
	}

	fmt.Println("Servidor Local:9096")
	server.ListenAndServe()
}
