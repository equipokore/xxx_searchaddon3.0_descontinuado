package MBuscar

import (
	"bytes"
	"context"
	"crypto/md5"
	"encoding/base64"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"html/template"
	"image/jpeg"
	"math/rand"
	"os"
	"strconv"
	"strings"
	"time"

	"../../Modulos/Conexiones"
	"../../Modulos/Generales"
	"gopkg.in/mgo.v2/bson"
	"gopkg.in/olivere/elastic.v5"
)

//SBusqueda Contiene el estatus y el mensaje que se mostrara en pantalla
type SBusqueda struct {
	SEstado bool
	SMsj    template.HTML
}

//OrdenRequerido determina si se requiere un ordenamiento en especifico
type OrdenRequerido struct {
	Campo string
	Orden bool
}

//Usuario estructura que contiene los datos minimos de un usuario
type Usuario struct {
	ID       bson.ObjectId `bson:"_id,omitempty"`
	Nombre   string        `bson:"nombre"`
	Username string        `bson:"username"`
	Userpass string        `bson:"password"`
	Nivel    string        `bson:"nivel"`
	Correo   string        `bson:"correo"`
}

/*
//ResultadoElastic datos de elastic
type ResultadoElastic struct {
	Clave       string  `json:"clave"`
	Nombre      string  `json:"nombre"`
	Codigo      string  `json:"codigobarra"`
	Modelo      string  `json:"modelo"`
	Unidad      string  `json:"unidadprimaria"`
	Marca       string  `json:"marca"`
	Linea       string  `json:"linea"`
	Precio      float64 `json:"precio"`
	Existencia  float64 `json:"existencia"`
	Tags        string  `json:"datos"`
	Operaciones int64   `Json:"numerooperaciones"`
}

//Resultado datos para consulta de elastic
type Resultado struct {
	ID          bson.ObjectId   `bson:"_id,omitempty"`
	Clave       string          `bson:"clave"`
	Nombre      string          `bson:"nombre"`
	Codigo      string          `bson:"codigobarra"`
	Modelo      string          `bson:"modelo"`
	Unidad      string          `bson:"unidadprimaria"`
	Marca       string          `bson:"marca"`
	Linea       string          `bson:"linea"`
	Precio      float64         `bson:"precio"`
	Existencia  float64         `bson:"existencia"`
	Operaciones int64           `bson:"numerooperaciones"`
	Tags        string          `bson:"datos"`
	Imagenes    []bson.ObjectId `bson:"imagenes,omitempty" `
	Archivos    []bson.ObjectId `bson:"archivos,omitempty" `
	TagImg      map[string]string
}
*/
//ResultadoFromElastico resultados devueltos por elastic
type ResultadoFromElastico struct {
	ID          string          `json:"_id"`
	Atzompa     float64         `json:"atzompa"`
	Clave       string          `json:"clave"`
	Cedis       float64         `json:"cedis"`
	Construcasa float64         `json:"construcasa"`
	Codigo      string          `json:"codigobarra"`
	Existencia  float64         `json:"existencias"`
	Ferrocarril float64         `json:"ferrocarril"`
	Hidalgo     float64         `json:"hidalgo"`
	Inventarios string          `json:"inventarios"`
	Linea       string          `json:"linea"`
	Marca       string          `json:"marca"`
	Modelo      string          `json:"modelo"`
	Nombre      string          `json:"nombre"`
	Operaciones int64           `json:"numerooperaciones"`
	Precio      float64         `json:"precio1"`
	Precio2     float64         `json:"precio2"`
	Precio3     float64         `json:"precio3"`
	Precio4     float64         `json:"precio4"`
	Precio5     float64         `json:"precio5"`
	Precio6     float64         `json:"precio6"`
	Reforma     float64         `json:"reforma"`
	Simbolos    float64         `json:"simbolos"`
	Tags        string          `json:"tagx"`
	Unidad      string          `json:"unidadprimaria"`
	UnidadSec   string          `json:"unidadsecundaria"`
	Imagenes    []bson.ObjectId `bson:"IMAGENES,omitempty"`
	// Archivos    []bson.ObjectId `json:"ARCHIVOS,omitempty"`
	TagImg map[string]string
}

//Feedback registro de consultas realizadas
type Feedback struct {
	Id                      bson.ObjectId `bson:"_id,omitempty"`
	CadenaBusqueda          string        `bson:"cadenabusqueda"`
	CadenaStringQueryQuotes string        `bson:"cadenastringqueryquotes"`
	CadenaStringQueryTilde  string        `bson:"cadenastringquerytilde"`
	NumeroResultados        int64         `bson:"numeroresultados"`
	HoraBusqueda            time.Time     `bson:"horabusqueda"`
	Resultados              bool          `bson:"resultados"`
	TipoQuery               string        `bson:"tipoquery"`
	Username                string        `bson:"usuario"`
	Userlvl                 string        `bson:"tipousuario"`
}

//GetAllUsuario funcion que obtiene todos los usuarios
func GetAllUsuario() []Usuario {
	var result []Usuario
	s, usuarios, err := Conexion.GetColectionMgo(Variable.ColeccionUsuario)
	if err != nil {
		fmt.Println(err)
	}
	err = usuarios.Find(nil).All(&result)
	if err != nil {
		fmt.Println(err)
	}
	s.Close()
	return result
}

//UpdateArt Actualizacion de los articulos
func UpdateArt(tags string, id bson.ObjectId) bool {
	var insertado bool
	s, c, err := Conexion.GetColectionMgo(Variable.ColeccionMetaArticulos)
	if err != nil {
		fmt.Println("Error al conectarse a la base de datos MongoDB", err)
	}
	defer s.Close()
	err = c.Update(bson.M{"_id": id}, bson.M{"$set": bson.M{"DATOS": tags}})
	if err != nil {
		fmt.Println("Erro al actualizar un articulo", err)
		insertado = false
	} else {
		insertado = true
	}
	return insertado
}

//UploadImageToMongodb Carga una imagen al directorio especificado
func UploadImageToMongodb(path string, namefile string) bson.ObjectId {
	file2, err := os.Open(path + "/" + namefile)
	if err != nil {
		fmt.Println("Error al abrir el archivo o el archivo no existe", err)
	}
	defer file2.Close()
	stat, err := file2.Stat()
	if err != nil {
		fmt.Println("Error al leer el archivo", err)
	}
	bs := make([]byte, stat.Size()) // read the file
	_, err = file2.Read(bs)
	if err != nil {
		fmt.Println("Error al crear objeto que contendrá el archivo", err)
	}
	/*
		db, err := Conexion.GetConexionMgo()
		if err != nil {
			fmt.Println("Error al conectar con mongo", err)
		}
	*/
	base := Conexion.GetBaseMgo()
	fmt.Println("Base de imagen: ", base)
	img, err := base.GridFS(Variable.ColeccionImagenes).Create(namefile)
	idsImg := img.Id()
	if err != nil {
		fmt.Println("error al crear archivo en mongo", err)
	}
	_, err = img.Write(bs)
	if err != nil {
		fmt.Println("error al escribir archivo en mongo", err)
	}
	err = img.Close()
	if err != nil {
		fmt.Println("error al cerrar img de mongo", err)
	}
	idimg := getObjectIdToInterface(idsImg)
	return idimg
}

//getObjectIdToInterface Obtiene un identificador de interface
func getObjectIdToInterface(i interface{}) bson.ObjectId {
	var v = i.(bson.ObjectId)
	return v
}

//UpdateImgArt Actualiza una imagen
func UpdateImgArt(idimg bson.ObjectId, id bson.ObjectId) {
	s, c, err := Conexion.GetColectionMgo(Variable.ColeccionMetaArticulos)
	if err != nil {
		fmt.Println("Error al conectarse a la base de datos MongoDB", err)
	}
	defer s.Close()
	err = c.Update(bson.M{"_id": id}, bson.M{"$push": bson.M{"IMAGENES": idimg}})
	if err != nil {
		panic(err)
	} else {
		fmt.Println("Imagen de Articulo Updated  :)")
	}
}

//UpdateArchArt Actualiza un archivo
func UpdateArchArt(idimg bson.ObjectId, id bson.ObjectId) {
	s, c, err := Conexion.GetColectionMgo(Variable.ColeccionMetaArticulos)
	if err != nil {
		fmt.Println("Error al conectarse a la base de datos MongoDB", err)
	}
	defer s.Close()

	err = c.Update(bson.M{"_id": id}, bson.M{"$push": bson.M{"ARCHIVOS": idimg}})

	if err != nil {
		panic(err)
	} else {
		fmt.Println("Archivo de Articulo Updated  :)")
	}
}

//BuscarEnElastic busca un texto específico en elástic
func BuscarEnElastic(texto string, filtros []string, ordenamiento string, name string, nivel string) (*elastic.SearchResult, error) {
	//conectamos a elastic search y nos traemos un tipo de dato *elastic.Client el cual tiene acceso a los indices que
	//se encuentran en la direccion establecida. en la conexion.
	client, err := Conexion.GetClienteElastic()
	//Comprobar si se pudo conectar a elasticsearch
	if err != nil {
		fmt.Println("Error en la conexion: ", err)
		return nil, err
	}

	//query string query
	var palabras = []string{}
	var final = []string{}
	var final2 = []string{}
	var cadena_final string
	var cadena_final2 string

	nueva_cadena := strings.Replace(texto, "/", "\\/", -1)
	nueva_cadena = strings.Replace(nueva_cadena, "~", "\\~", -1)
	nueva_cadena = strings.Replace(nueva_cadena, "^", "\\^", -1)
	nueva_cadena = strings.Replace(nueva_cadena, "+", "", -1)
	nueva_cadena = strings.Replace(nueva_cadena, "[", "", -1)
	nueva_cadena = strings.Replace(nueva_cadena, "]", "", -1)
	nueva_cadena = strings.Replace(nueva_cadena, "{", "", -1)
	nueva_cadena = strings.Replace(nueva_cadena, "}", "", -1)
	nueva_cadena = strings.Replace(nueva_cadena, "(", "\\(", -1)
	nueva_cadena = strings.Replace(nueva_cadena, ")", "\\)", -1)
	nueva_cadena = strings.Replace(nueva_cadena, "|", "", -1)
	nueva_cadena = strings.Replace(nueva_cadena, "=", "", -1)
	nueva_cadena = strings.Replace(nueva_cadena, ">", "", -1)
	nueva_cadena = strings.Replace(nueva_cadena, "<", "", -1)
	nueva_cadena = strings.Replace(nueva_cadena, "!", "", -1)
	nueva_cadena = strings.Replace(nueva_cadena, "&", "", -1)

	palabras = strings.Split(nueva_cadena, " ")
	for _, valor := range palabras {

		if valor != "" {
			palabrita := valor + `~2`
			final = append(final, palabrita)
		}
	}

	for _, valor := range palabras {

		if valor != "" {
			palabrita := "+" + `"` + valor + `"`
			final2 = append(final2, palabrita)
		}
	}

	for _, value := range final {
		cadena_final = cadena_final + " " + value
	}

	for _, value := range final2 {
		cadena_final2 = cadena_final2 + " " + value
	}

	// q2 := elastic.NewQueryStringQuery(" pala~2 AND (MARCA: TRUPER OR MARCA: CH) AND EXISTENCIA: >0 AND PRECIO: (>50 AND <200) ")

	var cadena_a_buscar string = ``
	var cadena_a_buscar2 string = ``

	cadena_a_buscar = ConstruirQueryString(cadena_final2, filtros)
	cadena_a_buscar2 = ConstruirQueryString(cadena_final, filtros)

	q1 := elastic.NewQueryStringQuery(cadena_a_buscar)

	q1 = q1.Field("nombre")
	q1 = q1.Field("codigobarra")
	//q1 = q1.Field("DATOS")
	q1 = q1.Field("marca")
	q1 = q1.Fuzziness("1")
	//q1 = q1.FieldWithBoost("DATOS", 2)
	q1 = q1.PhraseSlop(1)
	q1 = q1.AnalyzeWildcard(true)
	q1 = q1.FuzzyPrefixLength(1)

	q1 = q1.FuzzyMaxExpansions(1)

	q2 := elastic.NewQueryStringQuery(cadena_a_buscar2)

	q2 = q2.Field("nombre")
	q2 = q2.Field("codigobarra")
	//q2 = q2.Field("DATOS")
	q2 = q2.Field("marca")
	q2 = q2.Fuzziness("1")
	//q2 = q2.FieldWithBoost("DATOS", 2)
	q2 = q2.PhraseSlop(1)
	q2 = q2.AnalyzeWildcard(true)
	q2 = q2.FuzzyPrefixLength(1)

	q2 = q2.FuzzyMaxExpansions(1)

	agg := elastic.NewTermsAggregation().Field(Variable.DatoAgregacionElastic).OrderByTermDesc()

	var docs *elastic.SearchResult

	var array_orden []string
	var orden_stk OrdenRequerido

	array_orden = strings.Split(ordenamiento, ":")

	for kk, vv := range array_orden {
		if kk == 0 {
			orden_stk.Campo = vv
		} else {
			s, _ := strconv.ParseBool(vv)
			orden_stk.Orden = s
		}

	}

	var queryUsada string
	var erroresElastic bool
	if ordenamiento == "ELASTIC:true" {
		docs, err = client.Search().
			Index(Variable.IndexElastic).
			Type(Variable.TipoElasticArticulo).
			Query(q1).
			From(0).Size(10000).
			Do(context.TODO())

		if err != nil {
			fmt.Println("Ocurrio un error en la busqueda (elastic:true).", err)
		}
		queryUsada = "Q1"

		if docs.Hits.TotalHits == 0 {

			docs, err = client.Search().
				Index(Variable.IndexElastic).
				Type(Variable.TipoElasticArticulo).
				Query(q2).
				From(0).Size(10000).
				Do(context.TODO())
			if err != nil {

				fmt.Println("Ocurrio un error en la busqueda (elastic:true) Datos vacios.", err)
			}
			queryUsada = "Q2"
		}

	} else {
		queryUsada = "Q1"
		docs, err = client.Search().
			Index(Variable.IndexElastic).
			Type(Variable.TipoElasticArticulo).
			Query(q1).Aggregation(Variable.DatoAgregacionElastic, agg).
			Sort(orden_stk.Campo, orden_stk.Orden).
			From(0).Size(10000).
			Do(context.TODO())

		if err != nil {
			erroresElastic = true
			fmt.Println("Ocurrio un error en la busqueda (elastic:false).", err)
		} else {
			if docs.Hits.TotalHits == 0 {
				docs, err = client.Search().
					Index(Variable.IndexElastic).
					Type(Variable.TipoElasticArticulo).
					Query(q2).
					Sort(orden_stk.Campo, orden_stk.Orden).
					From(0).Size(10000).
					Do(context.TODO())
				if err != nil {
					fmt.Println("Ocurrio un error en la busqueda (elastic:false) Datos vacios.", err)
				}
				queryUsada = "Q2"
			}
		}
	}

	var nivelUsuario string

	switch nivel {
	case "0":
		nivelUsuario = "Administrador"
		break
	case "1":
		nivelUsuario = "Colaborador"
		break
	case "2":
		nivelUsuario = "Registrado"
		break
	case "3", "":
		nivelUsuario = "Invitado"
		break
	}
	defer client.CloseIndex(Variable.IndexElastic)
	if !erroresElastic {
		var datofeedback Feedback

		datofeedback.Id = bson.NewObjectId()
		datofeedback.CadenaBusqueda = texto
		datofeedback.CadenaStringQueryQuotes = cadena_a_buscar
		datofeedback.CadenaStringQueryTilde = cadena_a_buscar2
		datofeedback.NumeroResultados = docs.Hits.TotalHits
		datofeedback.HoraBusqueda = time.Now()
		datofeedback.TipoQuery = queryUsada
		datofeedback.Username = name
		datofeedback.Userlvl = nivelUsuario
		datofeedback.Resultados = true
		InsertarFeedback(datofeedback)
	}

	FlushElastic(client)
	return docs, err
}

//InsertarFeedback  Inserta un registro de consulta
func InsertarFeedback(dato Feedback) {
	s, c, err := Conexion.GetColectionMgo(Variable.ColeccionFeedback)
	if err != nil {
		fmt.Println("Error al conectarse a la base de datos MongoDB", err)
	}
	defer s.Close()
	err = c.Insert(dato)
	if err != nil {
		fmt.Println("Error al insertar el dato en feedback", err)
	}
}

/*
//GetArticles obtiene todos los articulos existentes
func GetArticles(datos []bson.ObjectId) []Resultado {
	var result []Resultado
	//variable ordenada del score de elasticsearch.
	var arregloOrdenado Resultado
	s, c, err := Conexion.GetColectionMgo(Variable.ColeccionMetaArticulos)
	if err != nil {
		fmt.Println("Error al conectarse a la base de datos MongoDB", err)
	}
	defer s.Close()

	for _, iddocumento := range datos {
		c.Find(bson.M{"_id": iddocumento}).One(&arregloOrdenado)
		result = append(result, arregloOrdenado)
	}

	return result

}
*/
//GetMarcas obtiene todas las marcas de un conjunto de ids
func GetMarcas(datos []ResultadoFromElastico) map[string]int {

	//var arr_int []int
	var arr_str []string
	var marcas = make(map[string]int)
	//contador = 0
	for key, valor := range datos {
		if key == 0 {
			//"Al inicio se inserta en el array de marcas"
			arr_str = append(arr_str, valor.Marca)
			marcas[valor.Marca] = 1
		} else {
			//"Cuando ya el array de marcas no este vacio, habrá que buscar si existe un elemento similar"
			numMarcas := busquedaMarcasTodas(arr_str, valor.Marca)
			if numMarcas > 0 {
				//"En caso de que ya exista un elemento, se ingresara la cantidad existente"
				marcas[valor.Marca] += numMarcas
			} else {
				//"Si no existe un valor similar, se insertará el nuevo elemento en el array de marcas"
				arr_str = append(arr_str, valor.Marca)
				marcas[valor.Marca] = 1
			}
		}
	}
	return marcas
}

func busquedaMarcasTodas(arr_str []string, elemento string) int {
	totalElementos := 0
	if arr_str != nil {
		for _, v := range arr_str {
			if v == elemento {
				totalElementos++
			}
		}
	}
	return totalElementos
}

//RegresaSrcImagenes Regresa un conjunto de Imagenes
func RegresaSrcImagenes(enc []ResultadoFromElastico) []ResultadoFromElastico {
	Base := Conexion.GetBaseMgo()
	for i, v := range enc {
		m := make(map[string]string)
		if len(v.Imagenes) > 0 {

			for _, xx := range v.Imagenes {

				img, err1 := Base.GridFS(Variable.ColeccionImagenes).OpenId(xx)
				if err1 != nil {
					if err1.Error() != "not found" {
						fmt.Println("Error al leer Imagen de MONGO: "+xx.Hex(), err1)
					}
				} else {
					b := make([]byte, img.Size())
					n, err := img.Read(b)

					if err != nil {
						fmt.Println("Error al crear mapa de bytes")
					}
					fmt.Println("N -> ", n)
					imagen, err := jpeg.Decode(bytes.NewReader(b))
					if err != nil {
						fmt.Println("Error al codificar.")
					}
					buffer := new(bytes.Buffer)

					err2 := jpeg.Encode(buffer, imagen, nil)
					if err2 != nil {
						fmt.Println("Error al codificar.")
					}
					str := base64.StdEncoding.EncodeToString(buffer.Bytes())
					m[xx.Hex()] = str
					defer img.Close()
				}
			}
			enc[i].TagImg = m
		}
	}
	return enc
}

//FlushElastic Depura la consulta en elasticsearch
func FlushElastic(client *elastic.Client) {
	_, err := client.Flush().Index(Variable.IndexElastic).Do(context.TODO())
	if err != nil {
		fmt.Println("Error al limpiar Objeto de consulta de elastic")
	}
}

/*
//Datosdearticulo obtiene los datos de un articulo preparado para elasticsearch
func Datosdearticulo(id bson.ObjectId) ResultadoElastic {
	var result ResultadoElastic
	var resultmongo Resultado
	s, c, err := Conexion.GetColectionMgo(Variable.ColeccionMetaArticulos)
	if err != nil {
		fmt.Println("Error al conectarse a la base de datos MongoDB", err)
	}
	c.Find(bson.M{"_id": id}).Select(bson.M{"_id": 0}).One(&resultmongo)

	result.Clave = resultmongo.Clave
	result.Nombre = resultmongo.Nombre
	result.Codigo = resultmongo.Codigo
	result.Modelo = resultmongo.Modelo
	result.Marca = resultmongo.Marca
	result.Unidad = resultmongo.Unidad
	result.Linea = resultmongo.Linea
	result.Precio = resultmongo.Precio
	result.Existencia = resultmongo.Existencia
	result.Tags = resultmongo.Tags
	result.Operaciones = resultmongo.Operaciones
	defer s.Close()
	return result
}
*/

//UpdateElascticValue Actualiza las etiquetas en elasticsearch
func UpdateElascticValue(index string, types string, id string, tags string) bool {
	client, err := Conexion.GetClienteElastic()
	var modificado bool
	if err != nil {
		fmt.Println(err)
	}
	res, err := client.Update().Index(index).Type(types).Id(id).Doc(map[string]interface{}{"tagx": tags}).DetectNoop(true).Do(context.TODO())
	if err != nil {
		fmt.Println(err)
	}
	if res.Created == false {
		fmt.Println("Encontrado y modificado")
		modificado = true
	} else {
		modificado = false
	}
	return modificado
}

//UpdateElascticValue Actualiza las etiquetas en elasticsearch
func UpdateIdImagesElasctic(index string, types string, id string, imagen []bson.ObjectId) bool {
	client, err := Conexion.GetClienteElastic()
	var modificado bool
	if err != nil {
		fmt.Println(err)
	}
	//res, err := client.Index().Index(index).Type(typ).Id(item.Id).BodyJson(item).Do(context.TODO())
	res, err := client.Update().Index(index).Type(types).Id(id).Doc(map[string]interface{}{"IMAGENES": imagen}).DetectNoop(true).Do(context.TODO())
	if err != nil {
		fmt.Println(err)
	}
	if res.Created == false {
		fmt.Println("Encontrado y modificado")
		modificado = true
	} else {
		modificado = false
	}
	return modificado
}

//UploadFileToMongodb Actualiza un archivo en mongodb
func UploadFileToMongodb(path string, namefile string) bson.ObjectId {
	db, err := Conexion.GetConexionMgo()
	if err != nil {
		fmt.Println("Error al conectar con mongo", err)
	}
	base := Conexion.GetBaseMgo()

	file2, err := os.Open(path + "/" + namefile)
	if err != nil {
		fmt.Println("Error al abrir el archivo o el archivo no existe")
	}
	defer file2.Close()
	stat, err := file2.Stat()
	if err != nil {
		fmt.Println("Error al leer el archivo")
	}
	bs := make([]byte, stat.Size()) // read the file
	_, err = file2.Read(bs)
	if err != nil {
		fmt.Println("Error al crear objeto que contendrá el archivo")
	}
	img, err := base.GridFS("ArchivosArticulos").Create(namefile)
	ids_img := img.Id()

	if err != nil {
		fmt.Println("error al crear archivo en mongo")
	}
	_, err = img.Write(bs)
	if err != nil {
		fmt.Println("error al escribir archivo en mongo")
	}
	err = img.Close()
	if err != nil {
		fmt.Println("error al cerrar img de mongo")
	}
	db.Close()

	idimg := getObjectIdToInterface(ids_img)

	return idimg

}

//ConstruirQueryString Construye la consulta para elasticsearch
func ConstruirQueryString(cadena_final string, filtros []string) string {

	var cadena string = ``

	cadena = cadena + cadena_final

	if len(filtros) != 0 {

		for _, filtrillo := range filtros {

			cadena = cadena + " AND " + filtrillo
		}
	}

	return cadena

}

//EliminarImg Elimina una imagen en mongodb
func EliminarImg(Idimg string) error {
	base := Conexion.GetBaseMgo()
	idImg := bson.ObjectIdHex(Idimg)
	err := base.GridFS(Variable.ColeccionImagenes).RemoveId(idImg)
	if err != nil {
		fmt.Println("Error al eliminar la imagen: ", err)
		return err
	}
	return err
}

//TraerObjetoElastico Trea productos de elastic pasandole la clave de dicho producto
func TraerObjetoElastico(clave string) (ResultadoFromElastico, error) {

	var ress ResultadoFromElastico

	resultadillo, err := BuscarEnElastic3(clave)
	if err != nil {
		fmt.Println("Ocurrio un error al buscar en elastic 3: ", err)
	} else {
		if resultadillo.Hits.TotalHits > 0 {

			for _, v := range resultadillo.Hits.Hits {

				err := json.Unmarshal(*v.Source, &ress)
				if err != nil {
					fmt.Println(err)
				}

			}
		}
	}
	return ress, err
}

//BuscarEnElastic3 Realiza una busqueda en elasticsearch de la clave dada
func BuscarEnElastic3(clave string) (*elastic.SearchResult, error) {
	client, err := Conexion.GetClienteElastic()
	if err != nil {
		fmt.Println("Error al conectar con elasticsearch: ", err)
		return nil, err
	} else {
		queryArticuloUnico := elastic.NewIdsQuery("maearticulos").Ids(clave).QueryName("QueryArticulo")

		articuloUnicoFromElastic, err := client.Search().
			Index(Variable.IndexElastic).
			Type(Variable.TipoElasticArticulo).
			Query(queryArticuloUnico).
			Do(context.TODO())
		if err != nil {
			fmt.Println("Ah ocurrido un error al realizar la busqueda")
		}
		return articuloUnicoFromElastic, err
	}
}

//ContarVecesUsado 	Cuenta el numero de veces que se realizo la consulta
func ContarVecesUsado() int {
	s, c, err := Conexion.GetColectionMgo(Variable.ColeccionFeedback)
	if err != nil {
		fmt.Println("Error al conectarse a la base de datos MongoDB", err)
	}
	defer s.Close()
	total, err := c.Find(nil).Count()
	if err != nil {
		fmt.Println(err)
	}
	return total
}

//EEl metodo de abajo se deja de usar, ya que la plantilla de sesion se usa desde el html
//ConstruirTemplateUsuario_SEDESUSA Constuye la plantilla que se mostrará en pantalla
/*
func ConstruirTemplateUsuario_SEDESUSA(userOn, nivel, id string) string {
	var templateUsuario string
	var nivelUsuario string

	switch nivel {
	case "0":
		nivelUsuario = "Usuario Kore"
		break
	case "1":
		nivelUsuario = "Administrador"
		break
	case "2":
		nivelUsuario = "Colaborador"
		break
	case "3":
		nivelUsuario = "Registrado"
		break
	case "4":
		nivelUsuario = "Invitado"
		break
	}

	nombres := strings.Split(userOn, " ")

	var nombreCortado string

	for ii, vv := range nombres {
		if ii == 0 {
			nombreCortado = vv + " "
		}

		if ii == 1 {
			vv2 := vv[0:1]
			nombreCortado += vv2
		}
	}

	if userOn == "Invitado" {
		templateUsuario += `
				<div class="sesiondeusuario">
					<div class="dropdown">
			    		<button class="botonlogeado dropdown-toggle" type="button" data-toggle="dropdown"> ` + userOn + `
			    		<span class="caret"></span></button>
				    		<ul class="dropdown-menu dropdown-menu-left">
			    	  			<li class="etiquetalogeo">` + nivelUsuario + `</li>
								<li class="divider"></li>
								<li><a class="etiquetalogeo" href="/">Buscar</a></li>
			    	  			<li class="divider"></li>
			      				<li><a class="etiquetalogeo" href="/logout">Cerrar Sesión</a></li>
			    			</ul>
			  		</div>
			  	</div>`

	} else if userOn != "Invitado" && userOn == "" {
				// templateUsuario += `
				// 		<div class="sesiondeusuario">
			    //     		<button class="botonbuscar" data-toggle="modal" data-target="#myModal5">Login</button>
			    // 		</div>`
	} else if userOn != "Invitado" && userOn != "" {

		fmt.Println("QUE ESTAMOS IMPRIMIENDO AQUI ->", userOn, nivelUsuario)

		templateUsuario += `
				<div class="sesiondeusuario">
					<div class="dropdown">
			    		<button class="botonlogeado dropdown-toggle" type="button" data-toggle="dropdown"> ` + nombreCortado + `
			    		<span class="caret"></span></button>
				    		<ul class="dropdown-menu dropdown-menu-left">
			    	  			<li class="etiquetalogeo">` + nivelUsuario + `</li>
								<li class="divider"></li>
								<li><a class="etiquetalogeo" href="/">Buscar</a></li>
			    	  			<li class="divider"></li>
			      				<li><a class="etiquetalogeo" href="/AdministrarCuenta/` + id + `">Cuenta</a></li>
			      				<li class="divider"></li>
			      				<li><a class="etiquetalogeo" href="/logout">Cerrar Sesión</a></li>
			    			</ul>
			  		</div>
			  	</div>`
	}

	if nivelUsuario == "Administrador" {
		templateUsuario = ``
		templateUsuario += `
				<div class="sesiondeusuario">
					<div class="dropdown">
			    		<button class="botonlogeado dropdown-toggle" type="button" data-toggle="dropdown"> ` + nombreCortado + `
			    		<span class="caret"></span></button>
				    		<ul class="dropdown-menu">
			    	  			<li class="etiquetalogeo">` + nivelUsuario + `</li>
								<li class="divider"></li>
								<li><a class="etiquetalogeo" href="/">Buscar</a></li>
			    	  			<li class="divider"></li>
			      				<li ><a class="etiquetalogeo" href="/AdministrarCuenta/` + id + `">Cuenta</a></li>
			      				<li class="divider"></li>
			      				<li><a  class="etiquetalogeo" href="/AdministrarUsuarios">Administrar</a></li>
			      				<li class="divider"></li>
			      				<li ><a class="etiquetalogeo" href="/logout">Cerrar Sesión</a></li>
			    			</ul>
			  		</div>
			  	</div>`

	}

	return templateUsuario
}
*/

//ComprobarUsuario Verifica si el usuario existe en la base de datos
func ComprobarUsuario(usuario string, password string) (*Usuario, bool) {
	s, c, err := Conexion.GetColectionMgo(Variable.ColeccionUsuario)
	if err != nil {
		fmt.Println("Error al conectarse a la base de datos MongoDB", err)
	}

	defer s.Close()

	var result Usuario

	bisonDocSearch := bson.M{"username": usuario, "password": password}

	err_f := c.Find(bisonDocSearch).Select(bson.M{"_id": 1, "username": 1, "password": 1, "nombre": 1, "nivel": 1}).One(&result)

	if err_f != nil {
		return nil, false
	} else {
		return &result, true
	}

}

//ExistMailandUpdate Actualiza un correo electronico en caso de existir
func ExistMailandUpdate(mail string) (string, error) {
	s, c, err := Conexion.GetColectionMgo(Variable.ColeccionUsuario)
	if err != nil {
		fmt.Println("Error al conectarse a la base de datos MongoDB", err)
	}
	defer s.Close()

	var result Usuario

	bisonDocSearch := bson.M{"correo": mail}
	err = c.Find(bisonDocSearch).Select(bson.M{"_id": 1, "username": 1, "password": 1, "correo": 1, "nombre": 1}).One(&result)

	if err == nil {
		secreto := RandStringRunes(6)
		hasher := md5.New()
		hasher.Write([]byte(secreto))
		secretomd5 := hex.EncodeToString(hasher.Sum(nil))
		bisonDocFilter := bson.M{"$set": bson.M{"password": secretomd5}}
		bisonDocSearch = bson.M{"_id": result.ID}
		_, err = c.UpdateAll(bisonDocSearch, bisonDocFilter)
		fmt.Println("ENTRANDO A: ", secreto, bisonDocSearch)
		return secreto, err

	}
	return "", err
}

//RandStringRunes XXXXXXXXXXXXXXXX(aun no se sabe que hace)
func RandStringRunes(n int) string {

	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}

var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")

//VerificarCorreo Corrobora que un correo existe
func VerificarCorreo(correo string, user string) (string, string) {

	s, c, err := Conexion.GetColectionMgo(Variable.ColeccionUsuario)
	if err != nil {
		fmt.Println("Error al conectarse a la base de datos MongoDB", err)
	}
	defer s.Close()

	var result Usuario
	var result2 Usuario
	var err2 error
	var correoexistente string
	var usuarioexistente string

	bisonDocSearch := bson.M{"correo": correo}
	err = c.Find(bisonDocSearch).Select(bson.M{"_id": 1, "correo": 1}).One(&result)

	bisonDocSearch2 := bson.M{"username": user}
	err2 = c.Find(bisonDocSearch2).Select(bson.M{"_id": 1, "username": 1}).One(&result2)

	fmt.Println("Correo Existente", result)
	fmt.Println("Usuario Existente", result2)

	if err == nil {
		correoexistente = result.Correo
	}

	if err2 == nil {
		usuarioexistente = result2.Username
	}

	return correoexistente, usuarioexistente

}

//VerificarCorreoId Verifica que un correo existe con el usuario especificado
func VerificarCorreoId(correo string, user string, id string) (string, string) {

	s, c, err := Conexion.GetColectionMgo(Variable.ColeccionUsuario)
	if err != nil {
		fmt.Println("Error al conectarse a la base de datos MongoDB", err)
	}
	defer s.Close()

	var result Usuario
	var result2 Usuario
	var err2 error
	var correoexistente string
	var usuarioexistente string

	bisonDocSearch := bson.M{"correo": correo, "_id": bson.M{"$ne": bson.ObjectIdHex(id)}}
	err = c.Find(bisonDocSearch).Select(bson.M{"_id": 1, "correo": 1}).One(&result)

	bisonDocSearch2 := bson.M{"username": user, "_id": bson.M{"$ne": bson.ObjectIdHex(id)}}
	err2 = c.Find(bisonDocSearch2).Select(bson.M{"_id": 1, "username": 1}).One(&result2)

	fmt.Println("Correo Existente", result)
	fmt.Println("Usuario Existente", result2)

	if err == nil {
		correoexistente = result.Correo
	}

	if err2 == nil {
		usuarioexistente = result2.Username
	}

	return correoexistente, usuarioexistente

}

//ConstruirTemplateCrearUsuario  Construye la plantilla correspondiente al nivel de usuario
func ConstruirTemplateCrearUsuario(nivel string, user Usuario) string {

	var tmpCrearUsuario string

	fmt.Println("IMPRIMIENDO EL NIVEL CON EL Q ACCEDEMOS AQUI -> ", nivel)

	tmpCrearUsuario += fmt.Sprintf(`

	<div class="container">

		<div class="page-header">
			<h1 class="etiquetaHeader">Crear usuario</h1>
		</div>
 	<br>
	<br>
	<br>
		`)

	tmpCrearUsuario += fmt.Sprintf(`
		<div class="row">
			<div class="col-lg-offset-3 col-md-offset-3 col-lg-6 col-md-6 col-sm-8 col-xs-8">
				<form method="POST" action="/registrarse" name="formregistrar" id="formregistrar">			
		`)

	tmpCrearUsuario += fmt.Sprintf(`
					<div class="form-group">
						<label class="etiquetalabeluser" for="nombreusuario">Nombre :</label>
						<div class="margeninputuser">
							<input value="%v" type="text" class="form-control etiquetainputuser" name="nombreusuario" id="nombreusuario" placeholder="Ingresa tu nombre" required="">
						</div>
					</div>
		`, user.Nombre)

	tmpCrearUsuario += fmt.Sprintf(`
					<div class="form-group">
						<label class="etiquetalabeluser" for="username">Usuario :</label>
						<div class="margeninputuser">
							<input value="%v" type="text" class="form-control etiquetainputuser" name="username" id="username" placeholder="Ingresa tu nombre" required="">
						</div>
					</div>
		`, user.Username)

	tmpCrearUsuario += fmt.Sprintf(`
					<div class="form-group">
						<label class="etiquetalabeluser" for="userpass1">Contraseña :</label>
						<div class="margeninputuser">
							<input type="password" class="form-control etiquetainputuser" name="userpass1" id="userpass1" placeholder="Ingresa tu contraseña" required=""  aria-describedby="contraseñaHelp0">
							<small id="contraseñaHelp0" class="form-text text-muted etiquetalabeluser">Debe contener 1 mayuscula, 1 minuscula , 1 numero y al menos 6 caracteres.</small>
						</div>
					</div>
		`)

	tmpCrearUsuario += fmt.Sprintf(`
					<div class="form-group">
						<label class="etiquetalabeluser" for="userpass2">Repetir Contraseña :</label>
						<div class="margeninputuser">
						<input type="password" class="form-control etiquetainputuser" name="userpass2" id="userpass2" aria-describedby="contraseñaHelp" placeholder="Repite tu contraseña" required="" >
						<small id="contraseñaHelp" class="form-text text-muted etiquetalabeluser">Las contraseñas deben coincidir</small>
						</div>
					</div>
		`)

	tmpCrearUsuario += fmt.Sprintf(`
					<div class="form-group">
						<label class="etiquetalabeluser" for="correoelectronico">Correo :</label>
						<div class="margeninputuser">
							<input value="%v" type="email" class="form-control etiquetainputuser" name="correoelectronico" id="correoelectronico" aria-describedby="correoHelp" placeholder="Ingresa tu correo" required="">
							<small id="correoHelp" class="form-text text-muted etiquetalabeluser">Este correo no se compartira con nadie.</small>
						</div>
					</div>
		`, user.Correo)

	if nivel == "0" || nivel == "1" {
		tmpCrearUsuario += fmt.Sprintf(`
						<div class="form-group">
							<label class="etiquetalabeluser" for="tipousuario">Tipo de Usuario :</label>
							<div class="margeninputuser">
								<select class="form-control" id="tipousuario">

									<option value="1">Administrador</option>
									<option value="2">Colaborador</option>
									<option value="3">Registrado</option>
									<option value="4">Invitado</option>
						
								</select>

							</div>
						</div>
		`)
	}

	tmpCrearUsuario += fmt.Sprintf(`
					<div class="form-group">
						<button type="submit" class="botonregistarusuario">Registrar</button>
						<button type="button"  onclick="if(confirm('¿Cancelar alta?'))window.location.href = '/'" class="botonregistarusuariocancel">Cancelar</button>
					</div>
		`)

	tmpCrearUsuario += fmt.Sprintf(`
				</form>
			</div>
		</div>
	</div>
		`)

	return tmpCrearUsuario

}

//ConstruirTemplateMensajes Construye una plantilla en base a los mensajes (errores)
func ConstruirTemplateMensajes(mensajes []string) string {

	var tmpMensajesError string

	tmpMensajesError += fmt.Sprintf(`
		<div class="informacionreserror">
			<div class="alert alert-warning alert-dismissible customAlert" role="alert">
    			<button type="button" class="customAlertClose" data-dismiss="alert" aria-label="Close">
    				<span aria-hidden="true">&times;</span>
    			</button>Se encontraron errores : 
    			`)

	for _, v := range mensajes {

		if v != "" {

			tmpMensajesError += fmt.Sprintf(` %v.. `, v)
		}

	}

	tmpMensajesError += fmt.Sprintf(`
    		</div>	
		</div>
		`)

	return tmpMensajesError
}

//GuardarUsuario guarda el usuario en la coleccion de usuarios
func GuardarUsuario(usuario Usuario) error {
	s, c, err := Conexion.GetColectionMgo(Variable.ColeccionUsuario)
	if err != nil {
		fmt.Println("Error al conectarse a la base de datos MongoDB", err)
	}
	defer s.Close()

	err = c.Insert(usuario)
	if err != nil {
		return err
	}
	return err
}

//ObtenerUsuario Obtiene algun usuario en la base de datos mongo
func ObtenerUsuario(ids string) Usuario {
	s, c, err := Conexion.GetColectionMgo(Variable.ColeccionUsuario)
	if err != nil {
		fmt.Println("Error al conectarse a la base de datos MongoDB", err)
	}
	defer s.Close()

	var user Usuario

	c.Find(bson.M{"_id": bson.ObjectIdHex(ids)}).One(&user)

	return user
}

//ConstruirTemplateModUsuario Funcion que construye una plantilla de acuerdo a un usuario especificado
func ConstruirTemplateModUsuario(usuario Usuario, usuarioLogeado string) string {

	temp := ``

	temp += fmt.Sprintf(`

	<div class="container">
		<br>
		<br>
		<div class="panel panel-default">
  			<div class="panel-body">
			  <h1 class="etiquetaHeader">Modificar Cuenta</h1>
			</div>
		</div>
 		<br>
		<br>
		<br>
		`)

	temp += fmt.Sprintf(`
		<div class="row">
			<div class="col-lg-offset-3 col-md-offset-3 col-lg-6 col-md-6 col-sm-8 col-xs-8">
				<form method="POST" action="/AdministrarCuenta/%v" name="formmodificar" id="formmodificar">			
		`, usuario.ID.Hex())

	temp += fmt.Sprintf(`
					<div class="form-group">
						<label class="etiquetalabeluser" for="nombreusuario">Nombre :</label>
						<div class="margeninputuser">
							<input value="%v" type="text" class="form-control etiquetainputuser" name="nombreusuario" id="nombreusuario" required="">
						</div>
					</div>
		`, usuario.Nombre)

	temp += fmt.Sprintf(`
					<div class="form-group">
						<label class="etiquetalabeluser" for="username">Usuario :</label>
						<div class="margeninputuser">
							<input value="%v" type="text" class="form-control etiquetainputuser" name="username" id="username" required="">
						</div>
					</div>
		`, usuario.Username)

	temp += fmt.Sprintf(`
					<div class="form-group">
							<button type="button" id="botonmostrarcontraseña" class="btn btn-secondary botoncambiarcontraseña" onclick="MostrarContraseña()">Cambiar Contraseña</button>
					</div>
		`)

	temp += fmt.Sprintf(`<div class="clearfix"></div>
						<br>`)

	temp += fmt.Sprintf(`<div id="cambiarcontraseña"  hidden="true">`)

	temp += fmt.Sprintf(`
					<div class="form-group">
						<label class="etiquetalabeluser" for="userpass">Contraseña Anterior:</label>
						<div class="margeninputuser">
							<input type="password" class="form-control etiquetainputuser" name="userpass" id="userpass" placeholder="Ingresa tu contraseña anterior">
						</div>
					</div>
		`)

	temp += fmt.Sprintf(`
					<div class="form-group">
						<label class="etiquetalabeluser" for="userpass1">Contraseña Nueva:</label>
						<div class="margeninputuser">
							<input type="password" class="form-control etiquetainputuser" name="userpass1" id="userpass1" placeholder="Ingresa tu contraseña nueva">
						</div>
					</div>
		`)

	temp += fmt.Sprintf(`
					<div class="form-group">
						<label class="etiquetalabeluser" for="userpass2">Repetir Contraseña Nueva:</label>
						<div class="margeninputuser">
						<input type="password" class="form-control etiquetainputuser" name="userpass2" id="userpass2" aria-describedby="contraseñaHelp" placeholder="Repite tu contraseña nueva">
						<small id="contraseñaHelp" class="form-text text-muted etiquetalabeluser">Las contraseñas deben coincidir</small>
						</div>
					</div>
		`)
	temp += fmt.Sprintf(`</div>`)

	temp += fmt.Sprintf(`
					<div class="form-group">
						<label class="etiquetalabeluser" for="correoelectronico">Correo :</label>
						<div class="margeninputuser">
							<input value="%v" type="email" class="form-control etiquetainputuser" name="correoelectronico" id="correoelectronico" aria-describedby="correoHelp" placeholder="Ingresa tu correo" required="">
							<small id="correoHelp" class="form-text text-muted etiquetalabeluser">Este correo no se compartira con nadie.</small>
						</div>
					</div>
		`, usuario.Correo)

	temp += fmt.Sprintf(`<div class="clearfix"></div>`)

	temp += fmt.Sprintf(CreateHtmlNivelUsuario(usuario.Nivel, usuarioLogeado))

	temp += fmt.Sprintf(`
					<div class="form-group">
						<button type="submit" form="formmodificar" class="botonregistarusuario" >Registrar</button>
						<button type="button"  onclick="if(confirm('¿Cancelar Modificación?'))window.location.href = '/'" class="botonregistarusuariocancel">Cancelar</button>
					</div>
		`)

	temp += fmt.Sprintf(`
				</form>
			</div>
		</div>
	</div>
		`)

	return temp

}

//CreateHtmlNivelUsuario funcion que registra
func CreateHtmlNivelUsuario(nivelUsuario string, nivelusuarioLogeado string) string {
	var levelUser string
	var options string

	if nivelUsuario == "0" {
		levelUser = `Administrador`
		options += fmt.Sprintf(`<option value="0" selected >Administrador</option>`)
	} else {
		options += fmt.Sprintf(`<option value="0">Usuario de Kore</option>`)
	}

	if nivelUsuario == "1" {
		levelUser = `Colaborador`
		options += fmt.Sprintf(`<option value="1" selected >Colaborador</option>`)
	} else {
		options += fmt.Sprintf(`<option value="1">Colaborador</option>`)
	}

	if nivelUsuario == "2" {
		levelUser = `Registrado`
		options += fmt.Sprintf(`<option value="2" selected >Registrado</option>`)
	} else {
		options += fmt.Sprintf(`<option value="2">Registrado</option>`)
	}

	if nivelUsuario == "3" {
		levelUser = `Invitado`
		options += fmt.Sprintf(`<option value="3" selected >Invitado</option>`)
	} else {
		options += fmt.Sprintf(`<option value="3">Invitado</option>`)
	}

	var html string
	if nivelusuarioLogeado == "0" {
		html += fmt.Sprintf(`
					<div class="form-group">
						<label class="etiquetalabeluser" for="nivelusuario">Nivel de Usuario :</label>
						<div class="margeninputuser" >
							<select name="nivelusuario" id="nivelusuario" class="form-control" >
								%v
							</select>
						</div>
					</div>
		`, options)

	} else {
		html += fmt.Sprintf(`
					<div class="form-group">
						<label class="etiquetalabeluser" for="nivelusuario">Nivel de Usuario :</label>
						<div class="margeninputuser">
							<input value="%v" type="text" class="form-control etiquetainputuser" name="nivelusuario" id="nivelusuario" readonly>
						</div>
					</div>
		`, levelUser)
	}
	return html
}

//GuardarUsuarioMod Actualia un usuario en mongodb
func GuardarUsuarioMod(usuario Usuario) error {
	s, c, err := Conexion.GetColectionMgo(Variable.ColeccionUsuario)
	if err != nil {
		fmt.Println("Error al conectarse a la base de datos MongoDB", err)
	}
	defer s.Close()

	err = c.Update(bson.M{"_id": usuario.ID}, usuario)
	if err != nil {
		return err
	}
	return err
}
