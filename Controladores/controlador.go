package controllers

import (
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"net/smtp"
	"strings"

	"../Controladores/Buscar"
	"../Modelos/Buscar"
	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2/bson"

	"./Session"
)

var cortarPrecio = buscar.CortarPrecio
var tmplIndex = template.Must(template.New("").Funcs(cortarPrecio).ParseFiles(
	"Vistas/index.html",
	"Vistas/Parciales/Plantilla/header.html",
	"Vistas/Parciales/Plantilla/footer.html",
	"Vistas/Parciales/Plantilla/styles.html",
	"Vistas/Parciales/Plantilla/scripts.html",
	"Vistas/Parciales/Paginas/Buscar/content.html",
))

//Index Muestra la pagina principal para comenzar la busqueda
func Index(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		name, nivel, IdUsuario := Session.GetUserName(r)
		if name == "" {
			searchtimes := MBuscar.ContarVecesUsado()
			tmplIndex.ExecuteTemplate(w, "index_layout", map[string]interface{}{
				//"SesionUsuario": template.HTML(templateUsuario),
				"VecesUsado": searchtimes,
			})
		} else {
			searchtimes := MBuscar.ContarVecesUsado()
			//templateUsuario := MBuscar.ConstruirTemplateUsuario(name, nivel, id)
			tmplIndex.ExecuteTemplate(w, "index_layout", map[string]interface{}{
				//"SesionUsuario": template.HTML(templateUsuario),
				"VecesUsado":   searchtimes,
				"NivelUsuario": nivel,
				"IdUsuario":    IdUsuario,
			})
		}
	}
}

//Login Redirecciona a la página para ingresar datos de acceso
func Login(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		userName := r.FormValue("userName")
		userPass := r.FormValue("userPass")
		redirectTarget := "/"
		if userName != "" && userPass != "" {
			var usuario *MBuscar.Usuario
			// var err error
			var userPassMd5 = userPass
			hasher := md5.New()
			hasher.Write([]byte(userPassMd5))
			userPassMd5 = hex.EncodeToString(hasher.Sum(nil))
			usuario, existe := MBuscar.ComprobarUsuario(userName, userPassMd5)

			if usuario == nil && existe == false {
				Session.SetSession("Invitado", "3", "", w)
				redirectTarget = "/"
			} else {
				Session.SetSession(usuario.Nombre, usuario.Nivel, usuario.ID.Hex(), w)
				redirectTarget = "/"
			}
		}
		http.Redirect(w, r, redirectTarget, 302)
	}
}

//Logout Funcion para salir del sistema
func Logout(w http.ResponseWriter, r *http.Request) {
	Session.ClearSession(w)
	http.Redirect(w, r, "/", 302)
}

//RecuperacionContraseña Funcion para recuperar la contraseña
func RecuperacionContraseña(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		r.ParseForm()
		correo := r.FormValue("correo")
		fmt.Println("correo: ", correo)

		if correo != "" {
			secreto, err := MBuscar.ExistMailandUpdate(correo)

			if err == nil {
				body := "<p>Estimado usuario:\n Su contrase&ntilde;a ha sido reestablecida al valor <strong>" + secreto + "</strong> .</p> \n Verifique su ingreso correcto."
				msg := "To: " + correo + "\r\n" +
					"Content-type: text/html" + "\r\n" +
					"Subject: Cambio de credenciales" + "\r\n\r\n" +
					body + "\r\n"
				// Set up authentication information.
				auth := smtp.PlainAuth(
					"",
					"test.miderp@hotmail.com",
					"D3m0st3n3s",
					"smtp.live.com",
				)
				// Connect to the server, authenticate, set the sender and recipient,
				// and send the email all in one step.
				err := smtp.SendMail(
					"smtp.live.com:587",
					auth,
					"test.miderp@hotmail.com",
					[]string{correo},
					[]byte(msg),
				)
				//err := smtp.SendMail("smtp.gmail.com:587", smtp.PlainAuth("", from, password, "smtp.gmail.com"), from, []string{to}, []byte(msg))
				if err != nil {
					log.Println(err)
					fmt.Fprintf(w, "Ha sucedido un error al procesar su solicitud,\n intente de nuevo.")
				} else {
					fmt.Fprintf(w, "Su solicitud ha sido procesada.")
				}
			} else {
				fmt.Println("User not found...")
				fmt.Fprintf(w, "Usuario no encontrado...")
			}

		} else {
			fmt.Fprintf(w, "Correo Invalido")
		}
	}

}

var tmpNewUser = template.Must(template.New("").ParseFiles(
	"Vistas/index.html",
	"Vistas/Parciales/Plantilla/header.html",
	"Vistas/Parciales/Plantilla/footer.html",
	"Vistas/Parciales/Plantilla/styles.html",
	"Vistas/Parciales/Plantilla/scripts.html",
	"Vistas/Parciales/Paginas/Buscar/createuser.html",
))

//CrearUsuario Funcion para crear un usuario
func CrearUsuario(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		var UsuarioDeAlta MBuscar.Usuario
		_, nivel, _ := Session.GetUserName(r)
		templateCrearUsuario := MBuscar.ConstruirTemplateCrearUsuario(nivel, UsuarioDeAlta)
		tmpNewUser.ExecuteTemplate(w, "index_layout", map[string]interface{}{
			"CrearUsuarioT": template.HTML(templateCrearUsuario),
		})
	} else if r.Method == "POST" {
		_, nivel, _ := Session.GetUserName(r)
		var errores bool
		var mensaje string
		nombreUsuario := r.FormValue("nombreusuario")
		userName := r.FormValue("username")
		userPass1 := r.FormValue("userpass1")
		userPass2 := r.FormValue("userpass2")
		correo := r.FormValue("correoelectronico")
		//tipousuario := r.FormValue("tipousuario")
		var UsuarioDeAlta MBuscar.Usuario
		correoExiste, usuarioExiste := MBuscar.VerificarCorreo(correo, userName)
		if correo == correoExiste {
			mensaje += "Correo existente,"
			errores = true
		}
		if usuarioExiste == userName {
			mensaje += "Usuario existente,"
			errores = true
		}
		if userPass1 != userPass2 {
			errores = true
			mensaje += "Contraseñas diferentes,"
		}
		UsuarioDeAlta.Username = userName
		UsuarioDeAlta.Nombre = nombreUsuario
		UsuarioDeAlta.Correo = correo

		if nivel == "" {
			//No existe alguien logeado y por lo tanto se le asigna el nivel default
			UsuarioDeAlta.Nivel = "2"
		}
		if errores == false {
			hasher := md5.New()
			hasher.Write([]byte(userPass1))
			secretomd5 := hex.EncodeToString(hasher.Sum(nil))
			UsuarioDeAlta.Userpass = secretomd5
			err := MBuscar.GuardarUsuario(UsuarioDeAlta)
			if err != nil {
				fmt.Println(err)
			} else {
				http.Redirect(w, r, "/", 302)
			}
		} else {
			mensajes := strings.Split(mensaje, ",")
			templateCrearUsuario := MBuscar.ConstruirTemplateCrearUsuario(nivel, UsuarioDeAlta)
			templateMensaje := MBuscar.ConstruirTemplateMensajes(mensajes)
			tmpNewUser.ExecuteTemplate(w, "index_layout", map[string]interface{}{
				"CrearUsuarioT": template.HTML(templateCrearUsuario),
				"MensajeT":      template.HTML(templateMensaje),
			})
		}
	}
}

var tmpModUser = template.Must(template.New("").ParseFiles(
	"Vistas/index.html",
	"Vistas/Parciales/Plantilla/header.html",
	"Vistas/Parciales/Plantilla/footer.html",
	"Vistas/Parciales/Plantilla/styles.html",
	"Vistas/Parciales/Plantilla/scripts.html",
	"Vistas/Parciales/Paginas/Buscar/moduser.html",
))

//ModificarCuenta Funcion para modificar una cuenta
func ModificarCuenta(w http.ResponseWriter, r *http.Request) {
	_, nivelUsuarioLogeado, IdUsuario := Session.GetUserName(r)
	fmt.Println("Modificando cuent acon el metodo: ", r.Method, IdUsuario)
	if r.Method == "GET" {
		variablesMux := mux.Vars(r)
		ids := variablesMux["ids"]
		var user MBuscar.Usuario
		user = MBuscar.ObtenerUsuario(ids)

		if IdUsuario == "" {
			http.Redirect(w, r, "/", 302)
		} else {
			templateModUsuario := MBuscar.ConstruirTemplateModUsuario(user, nivelUsuarioLogeado)
			//por ser una modificacion a la propia cuenta, no se puede cambiar a si mismo el nivel.. nadie puede...
			tmpModUser.ExecuteTemplate(w, "index_layout", map[string]interface{}{
				"ModificarUsuario": template.HTML(templateModUsuario),
				"NivelUsuario":     nivelUsuarioLogeado,
				"IdUsuario":        IdUsuario,
			})
		}
	} else if r.Method == "POST" {
		variablesMux := mux.Vars(r)
		ids := variablesMux["ids"]
		nombre := r.PostFormValue("nombreusuario")
		username := r.PostFormValue("username")
		userpass := r.PostFormValue("userpass")
		userpass1 := r.PostFormValue("userpass1")
		userpass2 := r.PostFormValue("userpass2")
		correo := r.PostFormValue("correoelectronico")
		nivelusuario := r.PostFormValue("nivelusuario")
		var usuariomodificado MBuscar.Usuario
		var usuariotemp MBuscar.Usuario
		var errores bool
		var mensaje string
		usuariotemp = MBuscar.ObtenerUsuario(ids)
		usuariomodificado.ID = bson.ObjectIdHex(ids)
		usuariomodificado.Nombre = nombre
		usuariomodificado.Username = username
		usuariomodificado.Correo = correo
		usuariomodificado.Nivel = nivelusuario

		if userpass == "" {
			usuariomodificado.Userpass = usuariotemp.Userpass
		} else {
			hasher := md5.New()
			hasher.Write([]byte(userpass))
			secretomd5 := hex.EncodeToString(hasher.Sum(nil))
			if secretomd5 == usuariotemp.Userpass {
				if userpass1 == userpass2 {
					hasher1 := md5.New()
					hasher1.Write([]byte(userpass1))
					secretomd52 := hex.EncodeToString(hasher1.Sum(nil))
					usuariomodificado.Userpass = secretomd52
				} else {
					mensaje += "Contraseña nueva no coincide,"
					errores = true
				}
			} else {
				mensaje += "Contraseña anterior incorrecta,"
				errores = true
			}
		}
		// correoExiste, usuarioExiste := MBuscar.VerificarCorreo(correo, username)
		correoExiste, usuarioExiste := MBuscar.VerificarCorreoId(correo, username, ids)
		if correo == correoExiste {
			mensaje += "Correo existente,"
			errores = true
		}
		if usuarioExiste == username {
			mensaje += "Usuario existente,"
			errores = true
		}

		if errores == true {
			//templateUsuario := MBuscar.ConstruirTemplateUsuario(name, nivel, id)
			mensajes := strings.Split(mensaje, ",")
			templateMensaje := MBuscar.ConstruirTemplateMensajes(mensajes)
			templateModUsuario := MBuscar.ConstruirTemplateModUsuario(usuariomodificado, nivelUsuarioLogeado)
			tmpModUser.ExecuteTemplate(w, "index_layout", map[string]interface{}{
				//"SesionUsuario":    template.HTML(templateUsuario),
				"MensajeT":         template.HTML(templateMensaje),
				"ModificarUsuario": template.HTML(templateModUsuario),
				"NivelUsuario":     nivelUsuarioLogeado,
				"IdUsuario":        IdUsuario,
			})
		} else {

			//aqui a guardar si no se encontraron errores
			fmt.Println("Ir a guardar este usuario no tiene errores la modificacion")

			err := MBuscar.GuardarUsuarioMod(usuariomodificado)
			if err != nil {
				fmt.Println(err)
			} else {
				if nivelUsuarioLogeado != "0" {
					Session.SetSession(usuariomodificado.Nombre, usuariomodificado.Nivel, usuariomodificado.ID.Hex(), w)
				}
				http.Redirect(w, r, "/", 302)
			}
		}
		fmt.Println(errores)
		fmt.Println(mensaje)

	}

}

var tmpAdminUser = template.Must(template.New("").ParseFiles(
	"Vistas/index.html",
	"Vistas/Parciales/Plantilla/header.html",
	"Vistas/Parciales/Plantilla/footer.html",
	"Vistas/Parciales/Plantilla/styles.html",
	"Vistas/Parciales/Plantilla/scripts.html",
	"Vistas/Parciales/Paginas/Buscar/adminuser.html",
))

//AdminUsers Funcion para administrar usuarios
func AdminUsers(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {

		//renderar no necesario para la peticion get...
		// tenemos q enviar los usarios correspondientes para editart...+

		//Segun la logica de programacion un admnistrador puede crear mas administradores, colaboradores, y cambiar estos niveles.
		//solo se le puede asignar un nivel de un combo???

		_, nivel, IdUsuario := Session.GetUserName(r)

		Usuarios := MBuscar.GetAllUsuario()
		//templateUsuario := MBuscar.ConstruirTemplateUsuario(name, nivel, id)

		tmpAdminUser.ExecuteTemplate(w, "index_layout", map[string]interface{}{
			//"SesionUsuario": template.HTML(templateUsuario),
			"Usuarios": Usuarios,
			// "ModificarUsuario": template.HTML(templateModUsuario),
			"NivelUsuario": nivel,
			"IdUsuario":    IdUsuario,
		})

	} else if r.Method == "POST" {

	}
}
