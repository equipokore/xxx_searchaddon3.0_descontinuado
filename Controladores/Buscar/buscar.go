package buscar

import (
	"encoding/json"
	"fmt"
	"html/template"
	"io"
	"net/http"
	"os"
	"sort"
	"strconv"
	"strings"

	"../../Modelos/Buscar"
	"../../Modulos/Generales"
	"../Session"
	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2/bson"
	elastic "gopkg.in/olivere/elastic.v5"
)

//CutPrice Recorta el precio a los minimos digitos
func CutPrice(num float64) float64 {
	str := strconv.FormatFloat(num, 'f', 2, 64)
	i, _ := strconv.ParseFloat(str, 64)
	return i
}

//CortarPrecio Variable que contieen el precio a ser recortado
var CortarPrecio = template.FuncMap{"CutPrice": CutPrice}

var tmpBuscar = template.Must(template.New("").Funcs(CortarPrecio).ParseFiles(
	"Vistas/index.html",
	"Vistas/Parciales/Plantilla/header.html",
	"Vistas/Parciales/Plantilla/footer.html",
	"Vistas/Parciales/Plantilla/styles.html",
	"Vistas/Parciales/Plantilla/scripts.html",
	"Vistas/Parciales/Paginas/Buscar/content.html",
))

//Variables globales en todo el controlador.
var NumeroRegistros int64
var Ids []string
var Ids_tmp []string
var NumPagina float32

//NumPagina especifica el numero de página en la que se cargarán los registros
var NumPaginaX int64
var lim int = 10
var limitePorPaginaBase int64 = 10
var totalpaginas int
var cadena_busqueda string
var cadena_busqueda_anterior string
var paginaactual int

//var result []MBuscar.Resultado
var resultElasticTotal []MBuscar.ResultadoFromElastico = nil

//var result_page []MBuscar.Resultado
var m = make(map[string]int)
var m_copy = make(map[string]int)
var msg string
var tmpl_pag = ``
var totalpages int
var template_ordenadores = ``
var template_filtros = ``

var refrescando string = ``
var template_filtros_copy string = ``
var arrayids3 []string
var arrProductoVista []MBuscar.ResultadoFromElastico
var docs *elastic.SearchResult

func Totalpaginas() int {

	NumPagina = float32(NumeroRegistros) / float32(lim)
	NumPagina2 := int(NumPagina)
	if NumPagina > float32(NumPagina2) {
		NumPagina2++
	}
	totalpaginas = NumPagina2
	return totalpaginas

}

func comprobarpagina(r *http.Request) int {
	return 0
}

func Busqueda(w http.ResponseWriter, r *http.Request) {
	name, nivel, IdUsuario := Session.GetUserName(r)
	//aqui cachamos la Funcion Buscar la cual se accesa con "/busqueda" en el metodo POST
	if r.Method == "POST" {
		Ids = nil
		msg = ""
		tmpl_pag = ``
		refrescando = ``
		template_ordenadores = ``
		template_filtros = ``

		arrProductoVista = nil

		var sBusqueda MBuscar.SBusqueda
		resultElasticTotal = nil
		var filtros []string
		var marcas []string
		var hitsstring string
		var tookstring string
		//obtenemos valores contenidos en el formulario que llega por POST
		cadena_busqueda = r.FormValue("searchbox")
		total_filtros := r.FormValue("total_filtros")
		ordenamiento := r.FormValue("orden")
		total_marcas := r.FormValue("total_marcas")
		informacion := r.FormValue("informacion")

		//Leemos la variable total filtros en string para posteriormente convertirla en entero e iterarla.
		if total_filtros != "0" {
			total_filtros_int, _ := strconv.Atoi(total_filtros)
			for i := 0; i < total_filtros_int; i++ {
				i_str := strconv.Itoa(i)
				filtros = append(filtros, r.FormValue("filtro_"+i_str))
			}
		}
		//leemos la variable total de marcas en string para posteriormente convertirla en entero e iterarla.
		if total_marcas != "0" {
			total_marcas_int, _ := strconv.Atoi(total_marcas)
			for i := 0; i < total_marcas_int; i++ {
				i_str := strconv.Itoa(i)
				marcas = append(marcas, r.FormValue("marca_"+i_str))
			}
		}

		//Preguntamos si la cadena de busqueda es igual a la cadena de busqueda anterior.
		//aqui se almacena la cadena que se teclea en el searcbox de la busqueda.
		// y asignamos la bandera "es_la_misma" true o false segun sea el caso.
		if cadena_busqueda_anterior != cadena_busqueda {
			m = nil
			arrProductoVista = nil
		}
		//Aqui preguntamos si la cadena del searchbox es diferente de "" (vacio)
		// y de contener una "busqueda" nos introducimos a este if.
		if cadena_busqueda != "" {
			//aqui vamos por los resultados de elastic search, se le envian datos de busqueda, filtros, ordenamiento y un puntero
			//al cliente de elastic search.
			//la variable "docs" contendra ya el resultado filtrado y ordenado por medio de metodos y querys dados dentro del metodo
			var err error
			docs, err = MBuscar.BuscarEnElastic(cadena_busqueda, filtros, ordenamiento, name, nivel)
			if err != nil {
				fmt.Println("Error en la conexion: ", err)
				sBusqueda.SEstado = false
				sBusqueda.SMsj = template.HTML("Error al conectarse al cliente: " + err.Error())
				msg = "Error al conectarse al cliente: " + err.Error()
			}
			if docs != nil {
				// si al regresar de la busqueda, obtenemos al menos 1 resultado ingresamos a este If > 0
				numeroResultados := docs.Hits.TotalHits
				if numeroResultados > 0 {
					//declaramos el numero de registros encontrados dentro de docs y sus metodos Hits.TotalHits
					NumeroRegistros = numeroResultados
					//definimos la variable total pagues la cual ejecuta el metodo TotalPaginas() el cual retorna el numero de paginas
					//necesarias para mostrar los resultados obtenidos se hacen calculos con otras variables globales.
					totalpages = Totalpaginas()
					//iteramos los resultados en su metodo Hits.Hits y obtenemos el campo Id. con "item.Id" con los cuales contruimos un
					//arreglo de string de puros Ids, para posteriormente buscarlos en mongodb.

					for _, v := range docs.Hits.Hits {
						//Producto = MBuscar.ResultadoFromElastico{}
						var Producto MBuscar.ResultadoFromElastico
						err := json.Unmarshal(*v.Source, &Producto)
						if err == nil {
							Producto.ID = v.Id
							resultElasticTotal = append(resultElasticTotal, Producto)
						} else {
							fmt.Println("Unmarchall Error: ", err)
						}
						iid := v.Id
						Ids = append(Ids, iid)
					}

					//Almacenar los datos en un arreglo de tal manera que se muestren en la vista el numero indicado de elementos
					if NumeroRegistros <= limitePorPaginaBase {
						for _, v := range resultElasticTotal[0:NumeroRegistros] {
							arrProductoVista = append(arrProductoVista, v)
						}
					} else if NumeroRegistros >= limitePorPaginaBase {
						for _, v := range resultElasticTotal[0:limitePorPaginaBase] {
							arrProductoVista = append(arrProductoVista, v)
						}
					}

					docs.Aggregations.Terms(Variable.DatoAgregacionElastic)
					//if encontrado {
					arrProductoVista = MBuscar.RegresaSrcImagenes(arrProductoVista)
					if cadena_busqueda_anterior != cadena_busqueda {
						m = MBuscar.GetMarcas(resultElasticTotal)
					}
					//convertimos los milisegundos a string
					//convertimos los registros totales a string
					hitsstring = strconv.FormatInt(docs.Hits.TotalHits, 10)
					tookstring = strconv.FormatInt(docs.TookInMillis, 10)
					//declaramos "msg" mensaje que se le envia al usuario por medio de template.
					msg = "\t " + hitsstring + " registros encontrados en " + tookstring + "  milisegundos\n"
					sBusqueda.SEstado = true
					sBusqueda.SMsj = template.HTML("\t " + hitsstring + " registros encontrados en " + tookstring + "  milisegundos\n")
					//asignamos la variable  "cadena_busqueda_anterior" el valor de la searchbox
					cadena_busqueda_anterior = cadena_busqueda
					/*
						} else {
							fmt.Println("NO SE ENCUENTRA...", ordenamiento)
							sBusqueda.SEstado = false
							sBusqueda.SMsj = template.HTML("No se encontraron coincidencias en la base de datos, vuelva a intentarlo")
							msg = "No se encontraron coincidencias en la base de datos, vuelva a intentarlo"
						}
					*/
				} else {

					// aqui entramos si no se encontraron resultados o si docs = 0
					msg = "No se han encontrado resultados para tu búsqueda."
					fmt.Println("No se encontró algún registro", msg)

					// preguntamos si llegamos aqui con la misma busqueda, en caso de llegar aqui con algun filtro
					// y volvemos a hacer la peticion para las marcas y no perder el arreglo de marcas filtradas en tal caso
					// asi como tambien creamos el filtro enviando m y la informacion.

					if cadena_busqueda_anterior == cadena_busqueda {
						template_filtros = CrearFiltros(m, informacion)
					}
				}
			} else {
				fmt.Println("Base de datos vacia....")
				msg = "Errores en la consulta"
			}

		} else {
			//aqio entramos cuando no especificas un criterio de busqueda, aunque con la validacion de Js no creo q entre aqui de nuevo.
			msg = "Debes especificar un criterio de busqueda."
			fmt.Println("No se especifico un criterio de busqueda", msg)
		}
		// A ESTA PARTE YA TENEMOS NUESTROS RESULTADOS CON IMAGENES Y MARCAS CON NOMBRE Y CANTIDAD ASI COMO ORDENADO Y FILTRADO POR Elastic

		// Entramos al proceso de paginacion el cual esta diseñado para mostrar paginas al inicio, al final y entre paginas.

		// Declaramos el inicio del template usado para paginacion el cual es una variable global
		//tmpl_pag = ConstruirPaginacion(totalpages, 1)
		tmpl_pag = ConstruirPaginacion(totalpages)

		template_ordenadores = CrearTemplateOrden(ordenamiento)
		template_filtros = CrearFiltros(m, informacion)

		if msg == "Debes especificar un criterio de busqueda." {
			fmt.Println("Opteniendo las marcas 1: criterio de busqueda")
			fmt.Println("mensaje ?= debes especificar un criterio")
			m = nil
			template_ordenadores = ``
			template_filtros = ``
		}

		searchtimes := MBuscar.ContarVecesUsado()

		//templateUsuario := MBuscar.ConstruirTemplateUsuario(name, nivel, id)
		if nivel == "" {
			nivel = "3"
		}
		var agregarinfo = 0
		//Se le crea las etiquetas de "agregar informacion extra", y "agregar imagenes" cuando se tiene los máximos privilegios
		if nivel == "0" {
			agregarinfo = 1
		}
		tmpBuscar.ExecuteTemplate(w, "index_layout", map[string]interface{}{
			"result":          arrProductoVista,
			"cadena_busqueda": cadena_busqueda,
			//"mensaje":         msg,
			"Estatus":     sBusqueda,
			"PaginacionT": template.HTML(tmpl_pag),
			"Ordenadores": template.HTML(template_ordenadores),
			"Filtros":     template.HTML(template_filtros),
			"Refrescar":   template.HTML(refrescando),
			//"SesionUsuario": template.HTML(templateUsuario),
			"NivelUsuario": nivel,
			"IdUsuario":    IdUsuario,
			"VecesUsado":   searchtimes,
			"Agregaciones": agregarinfo,
		})

	} else if r.Method == "GET" {
		tmpl_pag = ``
		//var arrayids []string
		variables_mux := mux.Vars(r)
		paginaactual_str := variables_mux["p"]

		if paginaactual_str == "" {
			tmpBuscar.ExecuteTemplate(w, "index_layout", nil)

		} else {
			paginaactual, _ = strconv.Atoi(paginaactual_str)

			result_page := MBuscar.RegresaSrcImagenes(arrProductoVista)
			result_page = ResultadosPaginacion(int64(paginaactual))

			tmpl_pag = ConstruirPaginacionGet(totalpages, paginaactual)

			if msg == "Debes especificar un criterio de busqueda." {
				tmpl_pag = ``
				m = nil
			}

			tmpBuscar.ExecuteTemplate(w, "index_layout", map[string]interface{}{
				"result":          result_page,
				"cadena_busqueda": cadena_busqueda,
				"mensaje":         msg,
				"PaginacionT":     template.HTML(tmpl_pag),
				"Ordenadores":     template.HTML(template_ordenadores),
				"IdUsuario":       IdUsuario,
				"Filtros":         template.HTML(template_filtros),
			})

		}
	}
}

func ConstruirPaginacion(totalpages int) string {
	tmpl_pag := `
			<nav aria-label="Page navigation">
  					<ul class="pagination">
    				<li>
      					<a href="/busqueda/1" aria-label="Primera">
        				<span aria-hidden="true">&laquo;</span>
      					</a>
    				</li>`

	for i := 1; i <= totalpages; i++ {

		if i == 1 {
			tmpl_pag += `<li class="active"><a href="/busqueda/` + strconv.Itoa(i) + `">` + strconv.Itoa(i) + `</a></li>`
		} else if i > 1 && i < 11 {
			tmpl_pag += `<li><a href="/busqueda/` + strconv.Itoa(i) + `">` + strconv.Itoa(i) + `</a></li>`
		} else if i > 11 && i == totalpages {
			tmpl_pag += `<li><span aria-hidden="true">...</span></li><li><a href="/busqueda/` + strconv.Itoa(i) + `">` + strconv.Itoa(i) + `</a></li>`
		}

	}

	tmpl_pag += `<li>
							<a href="/busqueda/` + strconv.Itoa(totalpages) + `" aria-label="Ultima">
        				<span aria-hidden="true">&raquo;</span>
      					</a>
    				</li>
  				</ul>
			</nav>
		`
	return tmpl_pag
}

func ConstruirPaginacionGet(totalpages int, paginaactual int) string {
	tmpl_pag := `
					<nav aria-label="Page navigation">
		  					<ul class="pagination">
		    				<li>
		      					<a href="/busqueda/1" aria-label="Primera">
		        				<span aria-hidden="true">&laquo;</span>
		      					</a>
		    				</li>`

	for i := 1; i <= totalpages; i++ {

		if paginaactual > 6 && i == 1 {
			tmpl_pag += `
									<li><a href="/busqueda/` + strconv.Itoa(i) + `">` + strconv.Itoa(i) + `</a></li>
									<li>
										<span aria-hidden="true">...</span>
									</li>
									`
		}

		switch {

		case paginaactual == i:
			if paginaactual == 1 {
				tmpl_pag += `<li class="active"><a href="/busqueda/` + strconv.Itoa(i) + `">` + strconv.Itoa(i) + `</a></li>`
			} else {
				tmpl_pag += `<li class="active"><a href="/busqueda/` + strconv.Itoa(i) + `">` + strconv.Itoa(i) + `</a></li>`
			}
			break
		case paginaactual == i-1:
			tmpl_pag += `<li><a href="/busqueda/` + strconv.Itoa(i) + `">` + strconv.Itoa(i) + `</a></li>`
			break

		case paginaactual == i-2:
			tmpl_pag += `<li><a href="/busqueda/` + strconv.Itoa(i) + `">` + strconv.Itoa(i) + `</a></li>`
			break

		case paginaactual == i-3:
			tmpl_pag += `<li><a href="/busqueda/` + strconv.Itoa(i) + `">` + strconv.Itoa(i) + `</a></li>`
			break
		case paginaactual == i-4:
			tmpl_pag += `<li><a href="/busqueda/` + strconv.Itoa(i) + `">` + strconv.Itoa(i) + `</a></li>`
			break
		case paginaactual == i-5:
			tmpl_pag += `<li><a href="/busqueda/` + strconv.Itoa(i) + `">` + strconv.Itoa(i) + `</a></li>`
			break

		case paginaactual == i+1:
			tmpl_pag += `<li><a href="/busqueda/` + strconv.Itoa(i) + `">` + strconv.Itoa(i) + `</a></li>`
			break

		case paginaactual == i+2:
			tmpl_pag += `<li><a href="/busqueda/` + strconv.Itoa(i) + `">` + strconv.Itoa(i) + `</a></li>`
			break

		case paginaactual == i+3:
			tmpl_pag += `<li><a href="/busqueda/` + strconv.Itoa(i) + `">` + strconv.Itoa(i) + `</a></li>`
			break
		case paginaactual == i+4:
			tmpl_pag += `<li><a href="/busqueda/` + strconv.Itoa(i) + `">` + strconv.Itoa(i) + `</a></li>`
			break
		case paginaactual == i+5:
			tmpl_pag += `<li><a href="/busqueda/` + strconv.Itoa(i) + `">` + strconv.Itoa(i) + `</a></li>`
			break
		}

		if i > paginaactual+6 && totalpages == i {
			tmpl_pag += `
										<li>
							  				<span aria-hidden="true">...</span>
										</li>
									<li><a href="/busqueda/` + strconv.Itoa(i) + `">` + strconv.Itoa(i) + `</a></li>`
		}

	}

	tmpl_pag += `<li>
									<a href="/busqueda/` + strconv.Itoa(totalpages) + `" aria-label="Ultima">
		        				<span aria-hidden="true">&raquo;</span>
		      					</a>
		    				</li>
		  				</ul>
					</nav>
				`
	return tmpl_pag
}

//ResultadosPaginacion regresa la tabla de busqueda y su paginacion en el momento de especificar página
func ResultadosPaginacion(Pagina int64) []MBuscar.ResultadoFromElastico {

	arrProductoVista = []MBuscar.ResultadoFromElastico{}
	if Pagina > 0 {
		if NumeroRegistros > 0 {
			NumPaginaX = Pagina
			skip := limitePorPaginaBase * (NumPaginaX - 1)
			limite := skip + limitePorPaginaBase

			if NumPaginaX == int64(totalpaginas) {
				final := NumeroRegistros % limitePorPaginaBase
				if final == 0 {
					for _, v := range resultElasticTotal[skip:limite] {
						arrProductoVista = append(arrProductoVista, v)
					}
				} else {
					for _, v := range resultElasticTotal[skip : skip+final] {
						arrProductoVista = append(arrProductoVista, v)
					}
				}

			} else {
				for _, v := range resultElasticTotal[skip:limite] {
					arrProductoVista = append(arrProductoVista, v)
				}
			}
		} else {
			fmt.Println("No se pueden mostrar páginas, no existen registros que mostrar.")
		}
	} else {
		fmt.Println("No hay páginas Cero.")
	}
	return arrProductoVista
}

func Modificar(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		var modificadoElastic bool
		id := r.FormValue("id")
		tags := r.FormValue("tags")
		/*
			obj, err := MBuscar.TraerObjetoElastico(id)
			if err != nil {
				fmt.Println("Error al consultar un objeto: ", err)
			}
		*/
		modificadoElastic = MBuscar.UpdateElascticValue(Variable.IndexElastic, Variable.TipoElasticArticulo, id, tags)
		if modificadoElastic {
			fmt.Fprintf(w, "Insertado")
		}

		//objectid := bson.ObjectIdHex(id)
		//modificado := MBuscar.UpdateArt(tags, id)
		/*
			for _, v := range docs.Hits.Hits {
				var art_elastico MBuscar.ResultadoFromElastico
				err := json.Unmarshal(*v.Source, &art_elastico)
				if err != nil {
					fmt.Println("Unmarchall Error: ", err)
				} else {
					modificadoElastic = MBuscar.UpdateElascticValue(Variable.IndexElastic, Variable.TipoElasticArticulo, id, tags)
					if modificadoElastic{
						fmt.Fprintf(w, "Insertado")
					}
				}
			}
		*/
	}
}

func check(err error, mensaje string) {
	if err != nil {
		panic(err)
	}
}

func Imagen(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		r.ParseMultipartForm(0)
		id_string := r.FormValue("d_id")
		file, header, err := r.FormFile("imagen[]")
		check(err, "Error al seleccionar la imagen")
		defer file.Close()
		nombrefile := header.Filename
		dir_path := "./Recursos/Local/Imagenes"
		if _, err := os.Stat(dir_path); os.IsNotExist(err) {
			os.MkdirAll(dir_path, 0777)
		}
		out, err := os.Create("./Recursos/Local/Imagenes/" + nombrefile)
		check(err, "Unable to create the file for writing. Check your write access privilege")
		defer out.Close()
		_, err = io.Copy(out, file)
		check(err, "Error al escribir la imagen al directorio")
		path_img := "./Recursos/Local/Imagenes"
		idsImg := MBuscar.UploadImageToMongodb(path_img, nombrefile)
		//objectid := bson.ObjectIdHex(id_string)
		//MBuscar.UpdateImgArt(idsImg, id_string)
		obj, err := MBuscar.TraerObjetoElastico(id_string)
		imagenesExistentes := obj.Imagenes
		if err != nil {
			fmt.Println("Error al consultar un objeto: ", err)
		} else {
			imagenesExistentes = append(imagenesExistentes, idsImg)
			insertado := MBuscar.UpdateIdImagesElasctic(Variable.IndexElastic, Variable.TipoElasticArticulo, id_string, imagenesExistentes)
			if insertado {
				fmt.Println("Insertado correctamente en elasticsearch")
			}
		}

		http.Redirect(w, r, "/del", 302)
	}
}

func EliminarDirectorio(w http.ResponseWriter, r *http.Request) {
	var er = os.RemoveAll("./Recursos/Local/Imagenes")
	check(er, "No se pudo eliminar el directorio")
	var refrescando = `	<script>
		$(document).ready(function() {Filtrado();});
			</script>`
	tmpBuscar.ExecuteTemplate(w, "index_layout", map[string]interface{}{
		"result":          arrProductoVista,
		"cadena_busqueda": cadena_busqueda,
		"mensaje":         msg,
		"PaginacionT":     template.HTML(tmpl_pag),
		"Ordenadores":     template.HTML(template_ordenadores),
		"Filtros":         template.HTML(template_filtros),
		"Refrescar":       template.HTML(refrescando),
	})

}

func EliminarDirectorio2(w http.ResponseWriter, r *http.Request) {
	var er = os.RemoveAll("./Recursos/Local/Archivos")
	check(er, "No se pudo eliminar el directorio")
	http.Redirect(w, r, "/", 302)

}

func Archivo(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		r.ParseMultipartForm(0)
		id_string := r.FormValue("d_id2")
		// name_string := r.FormValue("d_name2")
		file, header, err := r.FormFile("archivo[]")
		check(err, "Error al seleccionar el archivo")
		defer file.Close()
		nombrefile := header.Filename
		dir_path := "./Recursos/Local/Archivos"
		if _, err := os.Stat(dir_path); os.IsNotExist(err) {
			fmt.Println("el directorio no existe")
			os.MkdirAll(dir_path, 0777)
		} else {
			fmt.Println("el directorio ya existe")
		}
		out, err := os.Create("./Recursos/Local/Archivos/" + nombrefile)
		check(err, "Unable to create the file for writing. Check your write access privilege")
		defer out.Close()
		_, err = io.Copy(out, file)
		check(err, "Error al escribir la imagen al directorio")
		path_img := "./Recursos/Local/Archivos"
		idsImg := MBuscar.UploadFileToMongodb(path_img, nombrefile)
		fmt.Println("id de archivo: ", idsImg)
		objectid := bson.ObjectIdHex(id_string)
		MBuscar.UpdateArchArt(idsImg, objectid)
		http.Redirect(w, r, "/del2", 302)
	}
}

func CrearTemplateOrden(orden string) string {

	var template_checkboxs string = ``
	var arreglo_ordenado = []string{"precio1:true", "precio1:false", "numerooperaciones:true", "numerooperaciones:false", "existencias:true", "existencias:false", "ELASTIC:true"}

	template_checkboxs = template_checkboxs + `
		<div class="">
			<div class="">
				<form>`
	mapaUnchk := map[string]string{}
	mapaUnchk["precio1:true"] = `<label class="checkbox-inline etiquetasorden" for="menor_pr">&darr; precio</label><input type="checkbox" class="check_orden" onclick="Filtrado('precio1:true');" value="precio1:true" name="menor_pr" id="menor_pr">`
	mapaUnchk["precio1:false"] = `<label class="checkbox-inline etiquetasorden" for="mayor_pr">&uarr; precio</label><input type="checkbox" class="check_orden" onclick="Filtrado('precio1:false');" value="precio1:false" name="mayor_pr" id="mayor_pr">`
	mapaUnchk["numerooperaciones:true"] = `<label class="checkbox-inline etiquetasorden" for="menor_vta">&darr; ventas</label><input type="checkbox" class="check_orden" onclick="Filtrado('numerooperaciones:true');" value="numerooperaciones:true" name="menor_vta" id="menor_vta">`
	mapaUnchk["numerooperaciones:false"] = `<label class="checkbox-inline etiquetasorden" for="mayor_vta">&uarr; ventas</label><input type="checkbox" class="check_orden" onclick="Filtrado('numerooperaciones:false');" value="numerooperaciones:false" name="mayor_vta" id="mayor_vta">`
	mapaUnchk["existencias:true"] = `<label class="checkbox-inline etiquetasorden" for="menor_existe">&darr; existe</label><input type="checkbox" class="check_orden" onclick="Filtrado('existencias:true');" value="existencias:true" name="menor_existe" id="menor_existe">`
	mapaUnchk["existencias:false"] = `<label class="checkbox-inline etiquetasorden" for="mayor_existe">&uarr; existe</label><input type="checkbox" class="check_orden" onclick="Filtrado('existencias:false');" value="existencias:false" name="mayor_existe" id="mayor_existe">`
	mapaUnchk["ELASTIC:true"] = `<label class="checkbox-inline etiquetasorden" for="elastic_orden">Relevancia</label><input type="checkbox" class="check_orden" onclick="Filtrado('ELASTIC:true');" value="ELASTIC:true" name="elastic_orden" id="elastic_orden" >`

	mapaChk := map[string]string{}
	mapaChk["precio1:true"] = `<label class="checkbox-inline etiquetasorden orden_check" for ="menor_pr">&darr; precio</label><input type="checkbox" class="check_orden" onclick="Filtrado('precio1:true');" value="precio1:true" name="menor_pr" id="menor_pr" checked>`
	mapaChk["precio1:false"] = `<label class="checkbox-inline etiquetasorden orden_check" for="mayor_pr"><input type="checkbox" class="check_orden" onclick="Filtrado('precio1:false');" value="precio1:false" name="mayor_pr" id="mayor_pr" checked>&uarr; precio</label>`
	mapaChk["numerooperaciones:true"] = `<label class="checkbox-inline etiquetasorden orden_check" for="menor_vta"><input type="checkbox" class="check_orden" onclick="Filtrado('numerooperaciones:true');" value="numerooperaciones:true" name="menor_vta" id="menor_vta" checked>&darr; ventas</label>`
	mapaChk["numerooperaciones:false"] = `<label class="checkbox-inline etiquetasorden orden_check" for="mayor_vta"><input type="checkbox" class="check_orden" onclick="Filtrado('numerooperaciones:false');" value="numerooperaciones:false" name="mayor_vta" id="mayor_vta" checked>&uarr; ventas</label>`
	mapaChk["existencias:true"] = `<label class="checkbox-inline etiquetasorden orden_check" for="menor_existe"><input type="checkbox" class="check_orden" onclick="Filtrado('existencias:true');" value="existencias:true" name="menor_existe" id="menor_existe" checked>&darr; existe</label>`
	mapaChk["existencias:false"] = `<label class="checkbox-inline etiquetasorden orden_check" for="mayor_existe"><input type="checkbox" class="check_orden" onclick="Filtrado('existencias:false');" value="existencias:false" name="mayor_existe" id="mayor_existe" checked>&uarr; existe</label>`
	mapaChk["ELASTIC:true"] = `<label class="checkbox-inline etiquetasorden orden_check" for="elastic_orden"><input type="checkbox" class="check_orden" onclick="Filtrado('ELASTIC:true');" value="ELASTIC:true" name="elastic_orden" id="elastic_orden" checked>Relevancia</label>`

	for _, item := range arreglo_ordenado {
		if orden == item {
			template_checkboxs = template_checkboxs + mapaChk[item]
		} else {
			template_checkboxs = template_checkboxs + mapaUnchk[item]
		}
	}

	template_checkboxs = template_checkboxs + `</form></div></div>`

	return template_checkboxs

}

func CrearFiltros(marcas map[string]int, informacion string) string {

	var ss1 []string
	ss1 = strings.Split(informacion, "&")

	mapita := make(map[string]string)

	for _, v := range ss1 {
		if v != "" {
			ss2 := strings.Split(v, ":")
			mapita[ss2[0]] = ss2[1]
		}
	}
	var p_inf []string
	var precio_menor string
	var precio_mayor string

	p_inf = strings.Split(mapita["precio1"], ",")

	for mm, nm := range p_inf {
		if mm == 0 {
			precio_menor = nm
		} else if mm == 1 {
			precio_mayor = nm
		}
	}

	var marcas_array []string

	if mapita["marca"] != "" {
		marcas_array = strings.Split(mapita["marca"], ",")
	}

	var llaves []string

	for kk := range marcas {
		llaves = append(llaves, kk)
	}
	sort.Strings(llaves)

	template_filtros = `<div class="" >`

	template_filtros = template_filtros + `<div class="" >`

	if mapita["existencias"] == "true" {
		template_filtros = template_filtros + `
		<label class="etiquetafiltroexistenciachecked" for="filtroexistencia">Solo Existencia</label>
		<input  class="check_existencia" type="checkbox" onclick="Filtrado()" name="filtroexistencia" id="filtroexistencia" value="existencias:>0" checked>`
	} else {
		template_filtros = template_filtros + `
		<label class="etiquetafiltroexistencia" for="filtroexistencia">Solo Existencia</label>
		<input  class="check_existencia" type="checkbox" onclick="Filtrado()" name="filtroexistencia" id="filtroexistencia" value="existencias:>0">`

	}

	template_filtros = template_filtros + `</div>`

	template_filtros = template_filtros +
		`	<div class="clearfix"></div><br><div class="clearfix"></div>
			<span class="etiquetafiltro">Marcas:</span>
			<div class="clearfix"></div>
			<div class="contenedormarcasunicas">`

	// var conta_int int = 0
	var contador string

	for conta_int, nombre_marca := range llaves {
		verificar_marquita := VerificarMarca(marcas_array, nombre_marca)
		contador = strconv.Itoa(conta_int)
		cantidad_string := strconv.Itoa(marcas[nombre_marca])

		num_letras_marca := len(nombre_marca)

		var nombre_marca_acortada string = ``
		var marca_display string = ``

		if num_letras_marca > 12 {
			nombre_marca_acortada = nombre_marca[0:10]
			nombre_marca_acortada = nombre_marca_acortada + ".."
			marca_display = nombre_marca_acortada
		} else {
			marca_display = nombre_marca

		}

		if verificar_marquita == true {
			template_filtros = template_filtros + `
	 						<div class="contador_de_marca nombremarca-checked">
	 						<label class="nombremarca">
	 						<input class="check_marca" checked type="checkbox" onclick="Filtrado()" name="marca_` + contador + `" id="marca_` + contador + `" value="` + nombre_marca + `">
							&nbsp;&nbsp;` + marca_display + `
	 						</label>
	 						<label class="cantidadmarca">
								` + cantidad_string + `
	 						</label>
	 						<div class="clearfix"></div>
	 						</div>`

		} else {
			template_filtros = template_filtros + `
	 						<div class="contador_de_marca">
	 						<label class="nombremarca">
	 						<input class="check_marca" type="checkbox" onclick="Filtrado()" name="marca_` + contador + `" id="marca_` + contador + `" value="` + nombre_marca + `">
							&nbsp;&nbsp;` + marca_display + `
	 						</label>
	 						<label class="cantidadmarca">
								` + cantidad_string + `
	 						</label>
	 						<div class="clearfix"></div>
	 						</div>`
		}

	}

	template_filtros = template_filtros + `
			<span class="etiquetafiltro">Rango de precios:</span>
			<div class="clearfix"></div>`

	if len(p_inf) > 0 {
		template_filtros = template_filtros + `
		<input type="text" placeholder="Menor precio" id="rango_precio_menor" name="rango_precio_menor" value="` + precio_menor + `" class="filtrorango" pattern="\d+">
		<input type="text" placeholder="Mayor precio" id="rango_precio_mayor" name="rango_precio_mayor" value="` + precio_mayor + `" class="filtrorango" pattern="\d+">
		`
	} else {
		template_filtros = template_filtros + `
		<input type="text" placeholder="Menor precio" id="rango_precio_menor" name="rango_precio_menor" class="filtrorango" pattern="\d+">
		<input type="text" placeholder="Mayor precio" id="rango_precio_mayor" name="rango_precio_mayor" class="filtrorango" pattern="\d+">`
	}
	template_filtros = template_filtros + `	
		<div class="clearfix"></div>
		<input type="button" class="botonrango" onclick="Filtrado()"  name="filtro_rango_precio" id="filtro_rango_precio" value="Consultar">	
		<br><div class="clearfix"></div><br />`

	template_filtros = template_filtros + `</div></div>`

	return template_filtros
}

func VerificarMarca(marcas []string, marca string) bool {
	for _, b := range marcas {
		if b == marca {
			return true
		}
	}
	return false
}

func EliminarImagen(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		idImg := r.FormValue("idimg")
		errorEliminar := MBuscar.EliminarImg(idImg)
		if errorEliminar == nil {
			fmt.Fprintf(w, "eliminado")
		} else {
			fmt.Fprintf(w, "error: "+errorEliminar.Error())
		}
	}
}
