$(document).ready(function(){

$("#userPass").keypress(function(event) {
    if (event.which == 13) {
        event.preventDefault();
        $("#modalform5").submit();
    }
});

		$('#filer_input1').filer({
			showThumbs: true,
			addMore: true,
			allowDuplicates: false,
			extensions: ["pdf"],
			onSelect: function() {
    			$("#botonenviararch").prop('disabled', false);

			},
			onEmpty: function() {
    			$("#botonenviararch").prop('disabled', true);

			},
			captions : {
	    			button: "Subir Archivos",
	    			feedback: "archivos de tipo pdf.",
	    			feedback2: "Archivo(s) a subir.",
	    			drop: "Deposita aqui tu pdf",
	    			removeConfirmation: "¿Quieres remover este pdf?",
	    			errors: {
	        			filesLimit: "Only {{fi-limit}} files are allowed to be uploaded.",
	        			filesType: "Solo se permiten subir archivos pdf.",
	        			filesSize: "{{fi-name}} is too large! Please upload file up to {{fi-fileMaxSize}} MB.",
	        			filesSizeAll: "Files you've choosed are too large! Please upload files up to {{fi-maxSize}} MB.",
	        			folderUpload: "You are not allowed to upload folders."
	    				}
				}
		});

		$('#filer_input').filer({
			showThumbs: true,
			addMore: true,
			allowDuplicates: false,
			extensions: ["jpg", "png", "jpeg"],
			onSelect: function() {
			   	$("#botonenviarimg").prop('disabled', false);

			},
			onEmpty: function() {
			    $("#botonenviarimg").prop('disabled', true);

			},
			captions : {
	    			button: "Subir Imagenes",
	    			feedback: "archivos de tipo imagen.",
	    			feedback2: "Imagen(es) a subir.",
	    			drop: "Deposita aqui tu imagen.",
	    			removeConfirmation: "¿Quieres remover esta imagen?",
	    			errors: {
	        			filesLimit: "Only {{fi-limit}} files are allowed to be uploaded.",
	        			filesType: "Solo se permiten subir imagenes.",
	        			filesSize: "{{fi-name}} is too large! Please upload file up to {{fi-fileMaxSize}} MB.",
	        			filesSizeAll: "Files you've choosed are too large! Please upload files up to {{fi-maxSize}} MB.",
	        			folderUpload: "You are not allowed to upload folders."
	    				}
				}
		});

	$("body").on("click",".eliminar", function(e){
		$(this).parent('div').remove();
	});

	$('#myModal2').on('show.bs.modal', function(e) {
		var id = $(e.relatedTarget).data('id');    	
		$("#modalform input[name=d_id]").val(id);

		var name = $(e.relatedTarget).data('name');
		$("#modalform input[name=d_name]").val(name);
	});

	$('#myModal3').on('show.bs.modal', function(e) {
		var id = $(e.relatedTarget).data('id');    	
		$("#modalform2 input[name=d_id2]").val(id);

		var name = $(e.relatedTarget).data('name');
		$("#modalform2 input[name=d_name2]").val(name);
	});

	$('#myModal4').on('show.bs.modal', function(e) {
		var id = $(e.relatedTarget).data('id');    	
		$("#modalform3 input[name=article_id_tags]").val(id);

		var tags = $(e.relatedTarget).data('tags');
		$("#modalform3 input[name=article_tags]").val(tags);
	});


	$( ".carousel-inner" ).each(function() {
		$( ".carousel-inner div:first-child" ).addClass( "active" );
	});

	$("#inputs_generados").empty();



	$('#modalform5').on('show.bs.modal', function (e) {
		$('#userName').focus();
		alert("Hola mundo");
	});

	$('#modalform5').on('hidden.bs.modal', function (e) {
		  $('#searchbox').focus();
	});



});

function GetArticulo(id,name,cod,tags,clv){

   	$.ajax({
		url: '/articulodeelastic',
		type: 'POST',
		dataType: 'html',
		data:{clv:clv},
		success : function(data){
		$("#contenedor1").html(data);
		}
	});

	// $("#contenedor1").html('<form role="form" id="modal_form"><div class="form-group"><label for="nombre_modal">Nombre:</label><input type="text" class="form-control etiquetas5" id="nombre_modal" name="nombre_modal" value="'+name+'"" readonly="true" /></div><div class="form-group">	<label for="clave_modal">Clave:</label><input type="text" class="form-control etiquetas5" id="clave_modal" name="clave_modal" value="'+clv+'" readonly="true" /></div><div class="form-group">	<label for="clave_modal">Codigo:</label><input type="text" class="form-control etiquetas5" id="cod_modal" name="cod_modal" value="'+cod+'" readonly="true" /></div><div class="form-group"><input type="text" id="id_modal" name="id_modal" value="'+id+'" hidden="true" /></div> <div class="form-group"><label for="id_modal">Tags:</label><input type="text" class="form-control etiquetas5" id="modal_tags" name="modal_tags" value="'+tags+'" readonly="true" /> </form>');	

}

function MostrarDetalles(datos, NivelUsuario){	
	var json = JSON.parse(datos);
		var data=`<div class="row">
					<div class="well">
						<label for="nombre_modal">Nombre:</label>
						<input type="text" class="form-control etiquetas5" id="nombre_modal" name="nombre_modal" value='`+json.nombre+`' readonly="true" />							
						<br />
						<div class="col-lg-6 col-md-6">
							<label for="clave_modal">Clave:</label>
							<input type="text" class="form-control etiquetas5" id="clave_modal" name="clave_modal" value='`+json.clave+`' readonly="true" />
						</div>
						<div class="col-lg-6 col-md-6">
							<label for="codigo_modal">Codigo:</label>
							<input type="text" class="form-control etiquetas5" id="codigo_modal" name="codigo_modal" value='`+json.codigobarra+`' readonly="true" />
						</div>						
					`;
	switch(NivelUsuario) {
    	case "0":		
			data += `<br />
					<br />
					<hr>							
					<h4>Precios disponibles :</h4>
					<div class="clearfix"></div>							
					<div class="col-lg-4 col-md-4">
						<label for="precio1_modal">Precio 1:</label>
						<input type="text" class="form-control etiquetas5" id="precio1_modal" name="precio1_modal" value='`+json.precio1+`' readonly="true" />
					</div>
					<div class="col-lg-4 col-md-4">
						<label for="precio2_modal">Precio 2:</label>
						<input type="text" class="form-control etiquetas5" id="precio2_modal" name="precio2_modal" value='`+json.precio2+`' readonly="true" />
					</div>
					<div class="col-lg-4 col-md-4">
						<label for="precio3_modal">Precio 3:</label>
						<input type="text" class="form-control etiquetas5" id="precio3_modal" name="precio2_modal" value='`+json.precio3+`' readonly="true" />
					</div>
					<div class="col-lg-4 col-md-4">
						<label for="precio4_modal">Precio 4:</label>
						<input type="text" class="form-control etiquetas5" id="precio4_modal" name="precio2_modal" value='`+json.precio4+`' readonly="true" />
					</div>
					<div class="col-lg-4 col-md-4">
						<label for="precio5_modal">Precio 5:</label>
						<input type="text" class="form-control etiquetas5" id="precio5_modal" name="precio2_modal" value='`+json.precio5+`' readonly="true" />
					</div>
					<div class="col-lg-4 col-md-4">
						<label for="precio6_modal">Precio 6:</label>
						<input type="text" class="form-control etiquetas5" id="precio6_modal" name="precio6_modal" value='`+json.precio6+`' readonly="true" />
					</div>		
					<div class="clearfix"></div>
					<br />
					<br />
					<hr>
					<h4>Cantidades disponibles : `+json.existencias+`</h4>
					<div class="clearfix"></div>
					<div class="col-lg-4 col-md-4">
						<label for="atzompa_modal">Atzompa:</label>
						<input type="text" class="form-control etiquetas5" id="atzompa_modal" name="atzompa_modal" value="`+json.atzompa+` `+ json.unidadprimaria+`" readonly="true" />
					</div>
					<div class="col-lg-4 col-md-4">
						<label for="cedis_modal">Cedis:</label>
						<input type="text" class="form-control etiquetas5" id="cedis_modal" name="cedis_modal" value="`+json.cedis+` `+ json.unidadprimaria+`" readonly="true" />
					</div>
					<div class="col-lg-4 col-md-4">
						<label for="construcasa_modal">Construcasa:</label>
							<input type="text" class="form-control etiquetas5" id="construcasa_modal" name="construcasa_modal" value="`+json.construcasa+` `+ json.unidadprimaria+`" readonly="true" />
					</div>
					<div class="col-lg-4 col-md-4">
						<label for="ferrocarril_modal">Ferrocarril:</label>
						<input type="text" class="form-control etiquetas5" id="ferrocarril_modal" name="ferrocarril_modal" value="`+json.ferrocarril+` `+ json.unidadprimaria+`" readonly="true" />
					</div>
					<div class="col-lg-4 col-md-4">
						<label for="hidalgo_modal">Hidalgo:</label>
						<input type="text" class="form-control etiquetas5" id="hidalgo_modal" name="hidalgo_modal" value="`+json.hidalgo+` `+ json.unidadprimaria+`" readonly="true" />
					</div>
					<div class="col-lg-4 col-md-4">
						<label for="reforma_modal">Reforma:</label>
						<input type="text" class="form-control etiquetas5" id="reforma_modal" name="reforma_modal" value="`+json.reforma+` `+ json.unidadprimaria+`" readonly="true" />
					</div>
					<div class="col-lg-4 col-md-4">
						<label for="reforma_modal">Simbolos:</label>
						<input type="text" class="form-control etiquetas5" id="reforma_modal" name="reforma_modal" value="`+json.simbolos+` `+ json.unidadprimaria+`" readonly="true" />
					</div>
					<div class="clearfix"></div>
				</div>
			</div>`;					
        	break;
    	case "1":
			data += `
					<br />
					<br />
					<hr>
					<h4>Cantidades disponibles : `+json.existencias+`</h4>
					<div class="clearfix"></div>
					<div class="col-lg-4 col-md-4">
						<label for="atzompa_modal">Atzompa:</label>
						<input type="text" class="form-control etiquetas5" id="atzompa_modal" name="atzompa_modal" value="`+json.atzompa+` `+ json.unidadprimaria+`" readonly="true" />
					</div>
					<div class="col-lg-4 col-md-4">
						<label for="cedis_modal">Cedis:</label>
						<input type="text" class="form-control etiquetas5" id="cedis_modal" name="cedis_modal" value="`+json.cedis+` `+ json.unidadprimaria+`" readonly="true" />
					</div>
					<div class="col-lg-4 col-md-4">
						<label for="construcasa_modal">Construcasa:</label>
							<input type="text" class="form-control etiquetas5" id="construcasa_modal" name="construcasa_modal" value="`+json.construcasa+` `+ json.unidadprimaria+`" readonly="true" />
					</div>
					<div class="col-lg-4 col-md-4">
						<label for="ferrocarril_modal">Ferrocarril:</label>
						<input type="text" class="form-control etiquetas5" id="ferrocarril_modal" name="ferrocarril_modal" value="`+json.ferrocarril+` `+ json.unidadprimaria+`" readonly="true" />
					</div>
					<div class="col-lg-4 col-md-4">
						<label for="hidalgo_modal">Hidalgo:</label>
						<input type="text" class="form-control etiquetas5" id="hidalgo_modal" name="hidalgo_modal" value="`+json.hidalgo+` `+ json.unidadprimaria+`" readonly="true" />
					</div>
					<div class="col-lg-4 col-md-4">
						<label for="reforma_modal">Reforma:</label>
						<input type="text" class="form-control etiquetas5" id="reforma_modal" name="reforma_modal" value="`+json.reforma+` `+ json.unidadprimaria+`" readonly="true" />
					</div>
					<div class="col-lg-4 col-md-4">
						<label for="reforma_modal">Simbolos:</label>
						<input type="text" class="form-control etiquetas5" id="reforma_modal" name="reforma_modal" value="`+json.simbolos+` `+ json.unidadprimaria+`" readonly="true" />
					</div>
					<div class="clearfix"></div>
				</div>
			</div>`;					
        	break;			
    	case "2":
 			data += `<br />
							<br />
							<hr>
							<h4>Cantidades disponibles :  `+json.existencias+`</h4>
							<div class="clearfix"></div>
						</div>
					</div>`;
        	break;
    	case "3":
			data += 
					`<div class="clearfix"></div>
				</div>
			</div>`;			
        	break;		
    	default:
				data +=
				 `<div class="clearfix"></div>
				</div>
			</div>`;			
			break;				
	}	
	$("#contenedor1").html(data);
}

function UpdateArticulo(){

	var claves = ""
	var id = $("#article_id_tags").val();
	var tags = $("#article_tags").val();


    	$.ajax({
			url: '/modificararticulo',
			type: 'POST',
			dataType: 'html',
			data:{id:id, tags:tags},
			success : function(data){
				if (data  == "Insertado"){
					var modal = document.getElementById('myModal4');
					modal.style.display = "none";
					Filtrado();				
				}else{
					alert("Ocurrio un error");
				}
			}
		});
}

function copiarAlPortapapeles(id_elemento) {
  var aux = document.createElement("input");
  aux.setAttribute("value", id_elemento);
  document.body.appendChild(aux);
  aux.select();
  document.execCommand("copy");
  document.body.removeChild(aux);
}

function VisArchivos(id){
	alert(id);
}


function BorrarImagen(idimg,idart){
	
	$.ajax({
			url: '/eliminarimg',
			type: 'POST',
			dataType: 'html',
			data:{idart:idart, idimg:idimg},
			success : function(data){
				if (data  == "eliminado"){
					alert("Imagen Eliminada");
					Filtrado();
				}else{
					alert("Ocurrio un error");
				}
			}
		});


}


function VerImagen(Idimg,nombre,Iddoc){

							// Get the modal
							var modal = document.getElementById('myModal');

							// Get the image and insert it inside the modal - use its "alt" text as a caption

							id_img = Idimg+"_m";
							var img = document.getElementById(id_img);
							var modalImg = document.getElementById("img01");
							var captionText = document.getElementById("caption");



							img.onclick = function(){
							    modal.style.display = "block";
							    modalImg.src = this.src;
							    captionText.innerHTML = nombre;

							}

					
							// Get the <span> element that closes the modal
							var span = document.getElementsByClassName("closer_b")[0];

							// When the user clicks on <span> (x), close the modal
							span.onclick = function() {
							  modal.style.display = "none";
							}

							var trash = document.getElementsByClassName("eliminar_b")[0];

							trash.onclick = function() {
							  // modal.style.display = "none";

							  var r = confirm("¿eliminar?");
							  if (r == true) {
							  BorrarImagen(Idimg,Iddoc);
							  } else {
							      return false;
							  }

							}			  
}


function ConocerOrden(){

	var ordenamiento = "numerooperaciones:false"	

	menor_pr = $("#menor_pr").is(":checked");
	mayor_pr = $("#mayor_pr").is(":checked");
	menor_vta = $("#menor_vta").is(":checked");
	mayor_vta = $("#mayor_vta").is(":checked");
	menor_existe = $("#menor_existe").is(":checked");
	mayor_existe = $("#mayor_existe").is(":checked");
	nombre_orden = $("#nombre_orden").is(":checked");
	marca_orden = $("#marca_orden").is(":checked");
	elastic_orden = $("#elastic_orden").is(":checked");


	if (menor_pr == true)
		ordenamiento = $("#menor_pr").val();
	if (mayor_pr == true)
		ordenamiento = $("#mayor_pr").val();
	if (menor_vta == true)
		ordenamiento = $("#menor_vta").val();
	if (mayor_vta == true)
		ordenamiento = $("#mayor_vta").val();
	if (menor_existe == true)
		ordenamiento = $("#menor_existe").val();
	if (mayor_existe == true)
		ordenamiento = $("#mayor_existe").val();
	if (nombre_orden == true)
		ordenamiento = $("#nombre_orden").val();
	if (marca_orden == true)
		ordenamiento = $("#marca_orden").val();
	if (elastic_orden == true)
		ordenamiento = $("#elastic_orden").val();
	return ordenamiento

}

function FiltradoSolicitado(){
	
	var filtros = [];
	
	filtro_existencia = $("#filtroexistencia").is(":checked");

	if (filtro_existencia == true){
		filtrado = $("#filtroexistencia").val();
	 	filtros.push(filtrado)
	}

	return filtros
}

function PreciosSolicitados(){
	var precios = [];

	if ($("#rango_precio_menor").val() != ""){
		precios.push($("#rango_precio_menor").val())
	}
	if ($("#rango_precio_mayor").val() != ""){
		precios.push($("#rango_precio_mayor").val())
	}
	return	precios
}

function Filtrado(ordensillo){
	var informacion = [];		
	//SABER ORDEN ACTUAL
	x = ConocerOrden();
	busqueda = $("#searchbox").val();


	//SABER MARCHAS CHECKED
	var marcas = [];

	$(".contenedormarcasunicas div.contador_de_marca").each(function(){
		var marca = $(this).find(".check_marca").is(":checked"); 
			if (marca){
				var valor = $(this).find(".check_marca").val();
				marcas.push(valor);
			}
	});


	//SABER LOS FILTROS CHECKED
	filtros = FiltradoSolicitado();


	//SABER RANGO DE PRECIOS
	var cadena_precio = ""
	precios = PreciosSolicitados();

	if (precios.length){
		cadena_precio = 'precio1: (>'+precios[0]+' AND <'+precios[1]+')';
		filtros.push(cadena_precio);
	}

	cadena_marcas = '(marca: ';

	for (i=0; i<marcas.length;i++){
		if (i > 0){
			cadena_marcas = cadena_marcas + " OR marca: ";
		}
		cadena_marcas = cadena_marcas + marcas[i];
	}

	cadena_marcas = cadena_marcas + ')';

	if (marcas.length){
		filtros.push(cadena_marcas);
	}


	//CREAR INPUTS PARA EL FORMULARIO   - FILTROS -
	if (filtros.length){
		for (ii=0; ii<filtros.length; ii++){

			$('<input>').attr({
				type: 'text',
				id: 'filtro_'+ii,
			    name: 'filtro_'+ii,
			    hidden:true,
			    value:filtros[ii]
			}).appendTo('#inputs_generados');
		}

		$('<input>').attr({
				type: 'text',
				id: 'total_filtros',
			    name: 'total_filtros',
			    hidden:true,
			    value:filtros.length
			}).appendTo('#inputs_generados');		
	}

	//CREAR INPUTS PARA EL FORMULARIO   - MARCAS -
	if (marcas.length){
		for (iii=0; iii<marcas.length; iii++){

			$('<input>').attr({
				type: 'text',
				id: 'marca_'+iii,
			    name: 'marca_'+iii,
			    hidden:true,
			    value:marcas[iii]
			}).appendTo('#inputs_generados');
		}

		$('<input>').attr({
				type: 'text',
				id: 'total_marcas',
			    name: 'total_marcas',
			    hidden:true,
			    value:marcas.length
			}).appendTo('#inputs_generados');
	}

		if (ordensillo == undefined)
		{
			$("#orden").val(x);
			
		} else{
			$("#orden").val(ordensillo);
		}
		
	informacion[0]=RangosON();
	informacion[1]=ExistenciaON();
	informacion[2]=marcas;
	if (informacion[0] === false){
			alert("Algo fue mal con el rango");
			 $( "#rango_precio_menor" ).focus();
			return false;
	}

	var cadena_info = "";

	cadena_info = "precio1:"+informacion[0] +"&existencias:"+informacion[1] +"&marca:"+informacion[2];

		$('<input>').attr({
			type: 'text',
			id: 'informacion',
		    name: 'informacion',
		    hidden:true,
		    value:cadena_info
		}).appendTo('#inputs_generados');

	if (busqueda == ""){
		return false;
	} else{
	 	$("#searchform").trigger( "submit");
	}

}

function ExistenciaON(){
	var array_exist = []
	filtro_existencia = $("#filtroexistencia").is(":checked");	
	array_exist[0]=(filtro_existencia);		
	return array_exist;
}

function RangosON(){

	var rangos = [];

	rango_precio_menor=$("#rango_precio_menor").val();
	rango_precio_mayor=$("#rango_precio_mayor").val();

	if (rango_precio_menor != "" && rango_precio_mayor != ""){
		rangos[0] = rango_precio_menor;
		rangos[1] = rango_precio_mayor;

		if (rangos[0] >= rangos[1]){
			return false;
		}
	}

	return rangos;
}

function IniciarSesion(){
	$("#modalform5").trigger( "submit");
}

function RecuperarContraseña(){
	var correo = $("#CorreoRecovery").val();

	console.log("Correo : " + correo)

	$.ajax({
			url: '/passwordrecovery',
			type: 'POST',
			dataType: 'html',
			data:{correo:correo},
			success : function(data){
				console.log(data);
			}
		});


}

function MostrarSolicitud(){
	var isVisible = $('#solicitudcontraseña').is(':visible');
	var isHidden = $('#solicitudcontraseña').is(':hidden');

	if (isHidden) {
		$('#solicitudcontraseña').show();
	}else {
		$('#solicitudcontraseña').hide();
	}
}


function MostrarContraseña(){
	var isVisible = $('#cambiarcontraseña').is(':visible');
	var isHidden = $('#cambiarcontraseña').is(':hidden');

	if (isHidden) {
		$('#cambiarcontraseña').show();
		$('#botonmostrarcontraseña').html("Ocultar");
		$('#userpass').attr("required","true");
		$('#userpass1').attr("required","true");
		$('#userpass2').attr("required","true");
	}else {
		$('#cambiarcontraseña').hide();
		$('#botonmostrarcontraseña').html("Cambiar Contraseña");
		$('#userpass').removeAttr("required");
		$('#userpass1').removeAttr("required");
		$('#userpass2').removeAttr("required");
	}
}
