$(document).ready(function(){

$("#userPass").keypress(function(event) {
    if (event.which == 13) {
        event.preventDefault();
        $("#modalform5").submit();
    }
});


});

function IniciarSesion(){
	$("#modalform5").trigger( "submit");
}

function RecuperarContraseña(){
	var correo = $("#CorreoRecovery").val();

	$.ajax({
			url: '/passwordrecovery',
			type: 'POST',
			dataType: 'html',
			data:{correo:correo},
			success : function(data){
				console.log(data);
			}
		});
}

function MostrarSolicitud(){
	var isVisible = $('#solicitudcontraseña').is(':visible');
	var isHidden = $('#solicitudcontraseña').is(':hidden');

	if (isHidden) {
		$('#solicitudcontraseña').show();
	}else {
		$('#solicitudcontraseña').hide();
	}
}

function Registrarse() {
	window.location.href = "/registrarse"
}